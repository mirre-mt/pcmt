// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

type userSignin struct {
	Username string `form:"username" json:"username" validate:"required,gte=2"`
	Password string `form:"password" json:"password" validate:"required,gte=20"`
}

type userSignup struct {
	Username string `form:"username" json:"username" validate:"required,gte=2"`
	Email    string `form:"email" json:"email" validate:"required,email,gte=3"`
	Password string `form:"password" json:"password" validate:"required,gte=20"`
}

// this struct is also used on update by admins, which is why the password fields are omitempty.
// when users finish setting up, admins can no longer change their passwords.
type userCreate struct {
	Username       string `form:"username" json:"username" validate:"required,gte=2"`
	Email          string `form:"email" json:"email" validate:"required,email,gte=3"`
	Password       string `form:"password" json:"password" validate:"omitempty,gte=20,eqfield=RepeatPassword"`
	RepeatPassword string `form:"repeatPassword" json:"repeatPassword" validate:"omitempty,gte=20,eqfield=Password"`
	IsAdmin        bool   `form:"isAdmin" json:"isAdmin" validate:"omitempty"`
	IsActive       *bool  `form:"isActive" json:"isActive" validate:"omitempty"`
}

type userID struct {
	ID string `param:"id" validate:"required,uuid"`
}

type initPasswordChange struct {
	NewPassword       string `form:"new-password" validate:"required,gte=20,eqfield=RepeatNewPassword"`
	RepeatNewPassword string `form:"repeat-new-password" validate:"required,gte=20,eqfield=NewPassword"`
}

type hibpSearch struct {
	Account string `form:"search" validate:"required,gt=2"`
}

type hibpBreachDetail struct {
	BreachName string `param:"name" validate:"required,gt=0"`
}

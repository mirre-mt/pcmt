// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"net/http"

	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
)

func Logout() echo.HandlerFunc {
	return func(c echo.Context) error {
		defer addHeaders(c)

		switch {
		case c.Request().Method == "POST": // nolint:goconst
			sess, _ := session.Get(setting.SessionCookieName(), c)
			if sess != nil {
				log.Infof("max-age before logout: %d", sess.Options.MaxAge)
				sess.Options.MaxAge = -1

				delete(sess.Values, "username")

				err := sess.Save(c.Request(), c.Response())
				if err != nil {
					c.Logger().Error("could not delete session cookie")

					return renderErrorPage(
						c,
						http.StatusInternalServerError,
						http.StatusText(http.StatusInternalServerError),
						err.Error(),
					)
				}

				c.Response().Header().Set(echo.HeaderCacheControl, "no-store")
			}

			return c.Redirect(http.StatusMovedPermanently, "/logout")

		case c.Request().Method == "GET": // nolint:goconst
			sess, _ := session.Get(setting.SessionCookieName(), c)
			if sess != nil {
				if uname, ok := sess.Values["username"].(string); ok {
					if uname != "" {
						return c.Redirect(http.StatusSeeOther, "/home")
					}
				}
			}

			p := newPage()

			p.Title = "Logout"
			p.Current = "logout"

			err := c.Render(
				http.StatusOK,
				"logout.tmpl",
				p,
			)
			if err != nil {
				c.Logger().Errorf("error: %q", err)

				return renderErrorPage(
					c,
					http.StatusInternalServerError,
					http.StatusText(http.StatusInternalServerError),
					err.Error(),
				)
			}
		}

		return nil
	}
}

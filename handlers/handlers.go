// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"net/http"

	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
)

const unauthorisedAccess = "Unauthorised access detected"

func Index() echo.HandlerFunc {
	return func(c echo.Context) error {
		defer addHeaders(c)

		if sess, _ := session.Get(setting.SessionCookieName(), c); sess != nil {
			if uname, ok := sess.Values["username"].(string); ok {
				if uname != "" {
					return c.Redirect(http.StatusFound, "/home")
				}
			}
		}

		csrf := c.Get("csrf").(string)
		p := newPage()

		p.Title = "Welcome!"
		p.Current = "home"
		p.CSRF = csrf

		err := c.Render(
			http.StatusOK,
			"index.tmpl",
			p,
		)
		if err != nil {
			c.Logger().Errorf("error: %q", err)

			return renderErrorPage(
				c,
				http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError),
				err.Error(),
			)
		}

		return err
	}
}

func Healthz() echo.HandlerFunc {
	// TODO: have this respond with some kind of internal state that gets
	// read-locked when returning.
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, struct{ Status string }{Status: "OK"})
	}
}

func addHeaders(c echo.Context) {
	c.Response().Header().Set("Cross-Origin-Opener-Policy", "same-origin")
}

// experimental global redirect handler?
// http.HandleFunc("/redirect", func(w http.ResponseWriter, r *http.Request) {
// loc, err := url.QueryUnescape(r.URL.Query().Get("loc"))
// if err != nil {
// http.Error(w, fmt.Sprintf("invalid redirect: %q", r.URL.Query().Get("loc")), http.StatusBadRequest)
// return
// }
// http.Redirect(w, r, loc, http.StatusFound)
// })

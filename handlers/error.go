// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"errors"
	"fmt"
	"strconv"

	moduser "git.dotya.ml/mirre-mt/pcmt/modules/user"
	"github.com/labstack/echo/v4"
)

var (
	ErrNoSession        = errors.New("No session found, please log in")
	ErrSessionExpired   = errors.New("Session expired, log in again")
	ErrValidationFailed = errors.New("Check your input data")
)

func renderErrorPage(c echo.Context, status int, statusText, error string) error {
	defer addHeaders(c)

	strStatus := strconv.Itoa(status)

	p := newPage()

	p.Title = fmt.Sprintf("Error %s - %s", strStatus, statusText)
	p.Current = strStatus
	p.Error = error
	p.Status = strStatus
	p.StatusText = statusText

	u, ok := c.Get("sessUsr").(moduser.User)
	if ok {
		p.User = u
	}

	return c.Render(
		status,
		"errorPage.tmpl",
		p,
	)
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

type Iconier interface {
	Img() string
}

type Icon struct {
	img string
}

func (i *Icon) Img() string {
	return i.img
}

func NewIcon(data string) *Icon {
	return &Icon{img: data}
}

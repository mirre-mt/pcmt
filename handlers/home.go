// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"context"
	"net/http"

	"git.dotya.ml/mirre-mt/pcmt/ent"
	moduser "git.dotya.ml/mirre-mt/pcmt/modules/user"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
)

func Home(client *ent.Client) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer addHeaders(c)

		sess, _ := session.Get(setting.SessionCookieName(), c)
		if sess == nil {
			log.Info("no session, redirecting to /signin", "endpoint", "/home")
			return c.Redirect(http.StatusSeeOther, "/signin")
		}

		var username string

		username, ok := sess.Values["username"].(string)
		if !ok {
			log.Info("session cookie found but username invalid, redirecting to signin", "endpoint", "/home")
			return c.Redirect(http.StatusSeeOther, "/signin")
		}

		log.Debug("session", "username", username, "endpoint", "/home")

		var u moduser.User

		ctx := context.WithValue(context.Background(), moduser.CtxKey{}, slogger)
		if usr, err := moduser.QueryUser(ctx, client, username); err == nil && usr != nil {
			c.Logger().Debug("got usr: ", usr.Username)
			c.Logger().Debug("admin? ", usr.IsAdmin)

			u.ID = usr.ID
			u.Username = usr.Username
			u.IsActive = usr.IsActive
			u.IsAdmin = usr.IsAdmin
			// TODO: this is redundant, if there is a user object, the user is logged in...
			u.IsLoggedIn = true
		} else {
			c.Logger().Error("failed to query usr", username)

			return renderErrorPage(
				c,
				http.StatusInternalServerError,
				http.StatusText(http.StatusInternalServerError)+" failed to query usr (make sure you've got cookies enabled)",
				err.Error(),
			)
		}

		if !u.IsAdmin {
			f, err := moduser.UsrFinishedSetup(ctx, dbclient, u.ID)
			if err != nil {
				return renderErrorPage(
					c,
					http.StatusInternalServerError,
					http.StatusText(http.StatusInternalServerError),
					err.Error(),
				)
			}

			if !f {
				return c.Redirect(http.StatusSeeOther, "/user/initial-password-change")
			}
		}

		csrf := c.Get("csrf").(string)
		p := newPage()

		p.Title = "Home"
		p.Current = "home"
		p.CSRF = csrf
		p.Name = username
		p.User = u

		if flsh, ok := sess.Values["flash"].(string); ok {
			p.Data["flash"] = flsh

			delete(sess.Values, "flash")
		}

		if _, ok := sess.Values["reauthFlash"].(string); ok {
			p.Data["info"] = "First time after changing the password, yay!"
			// if this is the first login after the initial password change, delete
			// the cookie value.
			delete(sess.Values, "reauthFlash")
		}

		if err := sess.Save(c.Request(), c.Response()); err != nil {
			log.Error("Failed to save session", "module", "handlers/home")

			return renderErrorPage(
				c,
				http.StatusInternalServerError,
				http.StatusText(http.StatusInternalServerError)+" (make sure you've got cookies enabled)",
				err.Error(),
			)
		}

		err := c.Render(http.StatusOK, "home.tmpl", p)
		if err != nil {
			c.Logger().Errorf("error: %q", err)

			return renderErrorPage(
				c,
				http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError),
				err.Error(),
			)
		}

		return nil
	}
}

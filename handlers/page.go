// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import moduser "git.dotya.ml/mirre-mt/pcmt/modules/user"

type page struct {
	User       moduser.User
	AppName    string
	AppVer     string
	Title      string
	Name       string
	CSRF       string
	DevelMode  bool
	Current    string
	Error      string
	Status     string
	StatusText string
	Data       map[string]any
}

func newPage() *page {
	data := make(map[string]any, 0)

	data["RegistrationAllowed"] = registrationAllowed

	p := &page{
		AppName:   appName,
		AppVer:    appver,
		DevelMode: appIsDevel,
		Data:      data,
	}

	return p
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only
package handlers

import (
	"net/http"

	moduser "git.dotya.ml/mirre-mt/pcmt/modules/user"
	"github.com/labstack/echo/v4"
)

func ManageAPIKeys() echo.HandlerFunc {
	return func(c echo.Context) error {
		defer addHeaders(c)

		u, ok := c.Get("sessUsr").(moduser.User)
		if !ok {
			return renderErrorPage(
				c,
				http.StatusUnauthorized,
				http.StatusText(http.StatusUnauthorized),
				"username was nil",
			)
		}

		if !u.IsAdmin {
			c.Logger().Debug("this is a restricted endpoint", "endpoint", "/manage/api-keys", "user", u.Username, "isAdmin", u.IsAdmin)

			status := http.StatusForbidden
			msg := http.StatusText(status)

			return renderErrorPage(
				c, status, msg+": You should not be here", "Restricted endpoint",
			)
		}

		p := newPage()

		p.Title = "Manage API Keys"
		p.Current = "api-keys"
		p.User = u

		if setting.APIKeyHIBP() != "" {
			p.Data["hibpApiKey"] = setting.APIKeyHIBP()
		}

		if setting.APIKeyDehashed() != "" {
			p.Data["dehashedApiKey"] = setting.APIKeyDehashed()
		}

		err := c.Render(http.StatusOK, "manage/apikeys.tmpl",
			p,
		)
		if err != nil {
			c.Logger().Errorf("error: %q", err)

			return renderErrorPage(
				c,
				http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError),
				err.Error(),
			)
		}

		return nil
	}
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"context"
	"net/http"

	"git.dotya.ml/mirre-mt/pcmt/modules/hibp"
	moduser "git.dotya.ml/mirre-mt/pcmt/modules/user"
	"github.com/labstack/echo/v4"
)

func ViewHIBP() echo.HandlerFunc {
	return func(c echo.Context) error {
		defer addHeaders(c)

		u, ok := c.Get("sessUsr").(moduser.User)
		if !ok {
			c.Logger().Warnf("Error getting user from session cookie")
		}

		h := new(hibpBreachDetail)
		if err := c.Bind(h); err != nil {
			return renderErrorPage(
				c,
				http.StatusBadRequest,
				http.StatusText(http.StatusBadRequest),
				err.Error(),
			)
		}

		if err := c.Validate(h); err != nil {
			return renderErrorPage(
				c,
				http.StatusBadRequest,
				http.StatusText(http.StatusBadRequest)+" - "+ErrValidationFailed.Error(),
				err.Error(),
			)
		}

		ctx, ok := c.Get("sloggerCtx").(context.Context)
		if !ok {
			ctx = context.WithValue(context.Background(), hibp.CtxKey{}, slogger)
		}

		breachDetail, err := hibp.BreachForBreachName(ctx, dbclient, h.BreachName)
		if err != nil {
			return renderErrorPage(
				c,
				http.StatusInternalServerError,
				http.StatusText(http.StatusInternalServerError),
				err.Error(),
			)
		}

		p := newPage()

		p.Title = "HIBP breach details"
		p.Current = "hibp-breach-details"
		p.User = u
		p.Data["hibp"] = breachDetail

		err = c.Render(
			http.StatusOK,
			"user/hibp-details.tmpl",
			p,
		)
		if err != nil {
			c.Logger().Errorf("error: %q", err)

			return renderErrorPage(
				c,
				http.StatusInternalServerError,
				http.StatusText(http.StatusInternalServerError),
				err.Error(),
			)
		}

		return nil
	}
}

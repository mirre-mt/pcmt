// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"context"
	"net/http"

	"git.dotya.ml/mirre-mt/pcmt/ent"
	moduser "git.dotya.ml/mirre-mt/pcmt/modules/user"
	"github.com/gorilla/sessions"
)

func getUserByID(ctx context.Context, client *ent.Client, id string) (*ent.User, error) {
	usr, err := moduser.QueryUserByID(ctx, client, id)
	if err == nil && usr != nil {
		return usr, nil
	}

	return nil, err
}

func refreshSession(sess *sessions.Session, path string, maxAge int, httpOnly, secure bool, sameSite http.SameSite) {
	sess.Options = &sessions.Options{
		Domain:   setting.HTTPDomain(),
		Path:     path,
		MaxAge:   maxAge,
		HttpOnly: httpOnly,
		Secure:   secure,
		SameSite: sameSite,
	}
}

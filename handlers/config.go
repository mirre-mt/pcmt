// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package handlers

import (
	"git.dotya.ml/mirre-mt/pcmt/app/settings"
	"git.dotya.ml/mirre-mt/pcmt/ent"
	"git.dotya.ml/mirre-mt/pcmt/slogging"
	"golang.org/x/exp/slog"
)

var (
	setting             *settings.Settings
	appver              string
	appName             = "pcmt"
	appIsDevel          bool
	slogger             *slogging.Slogger
	log                 slogging.Slogger
	dbclient            *ent.Client
	registrationAllowed bool
)

func SetDBClient(client *ent.Client) {
	dbclient = client
}

func InitHandlers(s *settings.Settings) {
	slogger = slogging.Logger()
	log = *slogger // have a local copy.
	log.Logger = log.Logger.With(
		slog.Group("pcmt extra", slog.String("module", "handlers")),
	)

	setting = s

	appName = setting.AppName()
	appver = setting.Version()
	appIsDevel = setting.IsDevel()
	registrationAllowed = setting.RegistrationAllowed
}

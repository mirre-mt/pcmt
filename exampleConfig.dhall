{- import config schema that is integrity-check protected (with a fallback) -}
let ConfigSchema =
        https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
          sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50
      ? https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall

let Config = ConfigSchema.Schema

let c =
    {- below we allow overriding config values using env vars -}
      Config::{
      , Host = env:PCMT_HOST as Text ? Config.default.Host
      , LiveMode = env:PCMT_LIVE ? True
      , DevelMode = env:PCMT_DEVEL ? True
      , Session =
              Config.default.Session
          //  { CookieName = env:PCMT_SESSION_NAME as Text ? "pcmt_session"
              , CookieAuthSecret =
                    env:PCMT_SESSION_AUTH_SECRET as Text
                  ? Config.default.Session.CookieAuthSecret
              , CookieEncrSecret =
                    env:PCMT_SESSION_ENCR_SECRET as Text
                  ? Config.default.Session.CookieEncrSecret
              }
      , HTTP = Config.default.HTTP
      , Mailer = Config.default.Mailer
      , Init = Config.default.Init // { CreateAdmin = True }
      }

let _ =
    {- validate that the above adheres to the config schema and contains valid
       entries (not everything gets validated though).
    -}
      assert : ConfigSchema.Schema/validate c

in  c

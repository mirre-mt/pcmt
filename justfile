cmd := "podman"
cfile := "Containerfile"
tag := "docker.io/immawanderer/mt-pcmt:testbuild"
args := "build -t "+ tag + " " + buildargs + " --no-cache --pull -f " + cfile
buildargs := "--build-arg VERSION=" + vcs_ref + " --build-arg BUILD_DATE=" + build_date + " --build-arg VCS_REF=" + vcs_ref
kanikoargs := "run -it -w=" + kanikowdir + " -v $(pwd):" + kanikowdir + ":z " + kanikoexecutorimg + " -f=" + cfile + " -c=" + kanikocontext + " --use-new-run --snapshotMode=redo --no-push " + buildargs
kanikoexecutorimg := "gcr.io/kaniko-project/executor:v1.9.0-debug"
kanikowdir := "/src"
kanikocontext := "."
vcs_ref := "$(git rev-parse --short HEAD || echo dev)"
build_date := "$(date -u +\"%Y-%m-%dT%H:%M:%SZ\")"
hadolintimg := "docker.io/hadolint/hadolint"
hadolinttag := "v2.12.0-alpine"
hadolintargs := "run --rm -i -v $(pwd):/src:z --workdir=/src"

# run tailwindcss tool in watch mode.
watch-tw:
	npm i
	npx tailwindcss -i ./assets/input/css/tailwind.css -o ./assets/public/css/pcmt.css --watch

# start browser-sync.
watch-brs:
	npm i
	npx browser-sync start --config bs.js

# build app stylesheets using the tailwindcss cli tool.
tw:
	npm i
	npx tailwindcss -i ./assets/input/css/tailwind.css -o ./assets/public/css/pcmt.css --minify

# alias for tw.
frontend: tw

# build the application with debugging information.
build:
	go mod tidy
	go build -v -ldflags="-X main.version=$(git rev-parse --short HEAD)" .

# alias for build.
builddebug: build

# build the application without debugging information.
buildrelease:
	go mod tidy
	go build -v -trimpath -ldflags="-s -w -X main.version=$(git rev-parse --short HEAD)" .

# run the application.
run:
	./pcmt -devel

# build and run the application
dev: build run

# prepare the app for production.
prod: tw buildrelease

# generate code based on ent schemas.
gen:
	go generate -v ./ent

# add license headers to source code files.
addlicense:
	go run github.com/google/addlicense@v1.1.1 \
	-v -s=only \
	-c "wanderer <a_mirre at utb dot cz>" \
	-l "AGPL-3.0-only" \
	-ignore "tmp/**" -ignore "assets/**" -ignore "config/testdata/**" \
	-ignore "*.dhall" -ignore "node_modules/**" -ignore "devenv.yaml" \
	.

# start pg.
dbstart:
	mkdir -p tmp/db
	podman run \
	--name pg \
	--replace \
	--userns=keep-id \
	--rm \
	-it \
	-e POSTGRES_PASSWORD=postgres \
	-e POSTGRES_INITDB_ARGS="--auth-host=scram-sha-256 --auth-local=scram-sha-256" \
	-v $PWD/tmp/db:/var/lib/postgresql/data:Z \
	-p 127.0.0.1:5432:5432 \
	--health-cmd "sh -c 'pg_isready -U postgres -d postgres'" \
	--health-on-failure kill \
	--health-retries 3 \
	--health-interval 10s \
	--health-timeout 1s \
	--health-start-period=5s \
	docker.io/library/postgres:15.3-alpine3.18

# stop pg.
dbstop:
	podman stop pg

# process svg template assets.
svgo:
	svgo -i templates/svg-*.tmpl -i templates/svg/*.tmpl

# check Containerfile with hadolint.
hadolint:
	{{cmd}} {{hadolintargs}} {{hadolintimg}}:{{hadolinttag}} < {{cfile}}

# build Container with kaniko.
kaniko:
	{{cmd}} {{kanikoargs}}

# build container with podman.
container:
	{{cmd}} {{args}}

#  vim: set ts=2 ft=just syntax=make fenc=utf-8 ff=unix :

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package validation

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// Validator defines a validator that can be used with the echo framework.
type Validator struct {
	validator *validator.Validate
}

// New provides a new instance of type Validator, initialised with the default
// validator.
func New() *Validator {
	return &Validator{
		validator: validator.New(
			validator.WithRequiredStructEnabled(),
		),
	}
}

// Validate implements echo framework's Validator interface.
func (v *Validator) Validate(a any) error {
	if err := v.validator.Struct(a); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return nil
}

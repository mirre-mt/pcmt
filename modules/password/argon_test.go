// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package password

import (
	"testing"

	"github.com/matthewhartstonge/argon2"
)

func TestArgon(t *testing.T) {
	// set lighter defaults for the tests.
	argon = argon2.MemoryConstrainedDefaults()

	passwd := "password0123#@:"

	raw, err := Argon(passwd)
	if err != nil {
		t.Error(err)
	}

	// save the salt generated when hashing the password using our own func,
	// it'll be reused when reproducing manual hash creation.
	salt := raw.Salt

	manualRaw, err := argon.Hash([]byte(passwd), salt)
	if err != nil {
		t.Errorf("failed to create a hash: %q", err)
	}

	want := raw.Encode()
	got := manualRaw.Encode()

	t.Logf("want: %q, got: %q", want, got)

	if len(want) != len(got) {
		t.Errorf("password hashes differ, want: %q, got: %q", want, got)
	}

	for i := range got {
		if got[i] != want[i] {
			t.Logf("password hashes differ, want: %q, got: %q", want, got)
			break
		}
	}

	if _, err := ArgonVerify(passwd, want); err != nil {
		t.Errorf("passwords don't equal: %q", err)
	} else {
		t.Log("passwords equal")
	}

	if _, err := ArgonVerify(passwd, got); err != nil {
		t.Errorf("passwords don't equal for the manually created hash: %q", err)
	} else {
		t.Log("passwords equal for the manually created hash")
	}

	dw, err := ArgonDecode(want)
	if err != nil {
		t.Errorf("could not decode the digest: %q", err)
	}

	dg, err := ArgonDecode(got)
	if err != nil {
		t.Errorf("could not decode the digest: %q", err)
	}

	for i := range dw.Hash {
		if dw.Hash[i] != dg.Hash[i] {
			t.Errorf("hash values don't equal: want: %q, got: %q", dw.Hash, dg.Hash)
			break
		}
	}
}

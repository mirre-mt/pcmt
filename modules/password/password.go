// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package password

import "golang.org/x/crypto/bcrypt"

func GetHash(password string) ([]byte, error) {
	// NOTE: bcrypt will not operate on passwords longer than 72 characters.
	hash, err := bcrypt.GenerateFromPassword(
		[]byte(password), bcrypt.DefaultCost,
	)
	if err != nil {
		return nil, err
	}

	return hash, nil
}

func Compare(oldHash []byte, password string) bool {
	// NOTE: bcrypt will not operate on passwords longer than 72 characters.
	err := bcrypt.CompareHashAndPassword(oldHash, []byte(password))
	return err == nil
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package bluemonday

import "github.com/microcosm-cc/bluemonday"

var Policy = bluemonday.UGCPolicy()

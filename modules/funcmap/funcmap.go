// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package funcmap

import (
	"html/template"
	"io/fs"
	"strings"
	"time"

	modbluemonday "git.dotya.ml/mirre-mt/pcmt/modules/bluemonday"
)

var (
	embedAssets *fs.FS
	isLive      = true
)

func FuncMap() template.FuncMap {
	return template.FuncMap{
		"ifIE":    func() template.HTML { return template.HTML("<!--[if IE]>") },
		"endifIE": func() template.HTML { return template.HTML("<![endif]>") },
		"htmlSafe": func(html string) template.HTML {
			return template.HTML( //nolint:gosec
				modbluemonday.Policy.Sanitize(html),
			)
		},
		// a voluntarily unsafe func.
		"htmlRaw": func(value string) template.HTML {
			// nolint:gosec
			return template.HTML(value)
		},
		"htmlLinkStyle": func(value string) string {
			value = strings.ReplaceAll(value, "<a ", `<span><a class="w-auto py-1 mt-2 text-center text-blue-500 md:mt-0 mx-0 hover:underline dark:text-blue-400"`)
			return strings.ReplaceAll(value, "</a>", `</a></span>`)
		},
		"pageIs": func(want, got string) bool {
			return want == got
		},
		"sha256": func(path string) string {
			t := New("sha256")

			r, err := t.Integrity(path, isLive)
			if err != nil {
				return ""
			}

			return *r
		},
		"sha384": func(path string) string {
			t := New("sha384")

			r, err := t.Integrity(path, isLive)
			if err != nil {
				return ""
			}

			return *r
		},
		"sha512": func(path string) string {
			t := New("sha512")

			r, err := t.Integrity(path, isLive)
			if err != nil {
				return ""
			}

			return *r
		},
		"usrFinishedSetup": func(lastLogin time.Time) bool {
			return lastLogin.After(time.Unix(0, 0))
		},
	}
}

// SetEmbeds saves the pointer to embedded assets (and toggles the isLive var).
func SetEmbeds(embeds *fs.FS) {
	embedAssets = embeds
	isLive = false
}

// TODO: mimic https://github.com/drone/funcmap/blob/master/funcmap.go
func setFuncMap(t *template.Template) { //nolint:unused
	t.Funcs(FuncMap())
}

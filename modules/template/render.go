// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"html/template"
	"io"

	"github.com/labstack/echo/v4"
)

type TplRenderer struct {
	tmpls *template.Template
}

var Renderer = &TplRenderer{tmpls: template.New("")}

func (t *TplRenderer) Render(w io.Writer, name string, data any, c echo.Context) error {
	c.Logger().Debugf("rendering template %s", name)

	tpl := Get(name)

	return tpl.Execute(w, data)
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"io/fs"
	"path/filepath"
)

// globDir lists files inside a dir that match the suffix provided.
func globDir(dir, suffix string) ([]string, error) {
	files := []string{}

	err := filepath.WalkDir(dir, func(path string, de fs.DirEntry, err error) error {
		if de.IsDir() {
			return nil
		}

		if filepath.Ext(path) == suffix {
			files = append(files, path)
		}

		return nil
	})

	return files, err
}

// globFS lists files inside a dir of a fs that match the suffix provided.
func globFS(fspls fs.FS, dir, suffix string) ([]string, error) {
	files := []string{}

	err := fs.WalkDir(fspls, dir, func(path string, de fs.DirEntry, err error) error {
		if de.IsDir() {
			return nil
		}

		if filepath.Ext(path) == suffix {
			files = append(files, path)
		}

		return nil
	})

	return files, err
}

// listAll returns template file names as []string and panics on error.
// uses filepath.WalkDir via glob* funcs. TODO: don't panic.
func listAll(pattern string) []string {
	files := make([]string, 0)

	switch {
	case !setting.IsLive():
		f, err := globFS(tmplFS, ".", pattern)
		if err != nil {
			log.Errorf("error: %q", err)
			panic(err)
		}

		files = f

	case setting.IsLive():
		f, err := globDir(tmplPath, pattern)
		if err != nil {
			panic(err)
		}

		files = f
	}

	return files
}

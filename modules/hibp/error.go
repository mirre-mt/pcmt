// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package hibp

import "errors"

var (
	ErrAuthKeyCheckValue     = errors.New(authKeyCheckValue)
	ErrRateLimited           = errors.New("We have been rate limited")
	ErrBreachNotFound        = errors.New("Breach not found")
	ErrBreachNotSingular     = errors.New("Multiple breaches for one name")
	ErrFailedToQueryBreaches = errors.New("Failed to query breaches")
	ErrNoBreachesToSave      = errors.New("No breaches to save")
)

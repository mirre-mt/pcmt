// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package hibp

// CtxKey serves as a key to context values for this package.
type CtxKey struct{}

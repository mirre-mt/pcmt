// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package hibp

import (
	"errors"
	"io"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"testing"
	"time"
)

const hibpTestDomain = "@hibp-integration-tests.com"

func init() {
	os.Setenv("GO_ENV_TESTING", "1")
	prepScheduler() // purposely ignoring the output values.
}

func prepScheduler() (*chan os.Signal, *chan error, *sync.WaitGroup) {
	quit := make(chan os.Signal, 1)
	errCh := make(chan error)
	wg := &sync.WaitGroup{}

	signal.Notify(quit, os.Interrupt)

	schedInitNapDuration = 0 * time.Millisecond

	go func() {
		wg.Add(1)                           // nolint:staticcheck
		go RunReqScheduler(quit, errCh, wg) //nolint:wsl
	}()

	// give the scheduler some time to start up.
	time.Sleep(200 * time.Millisecond)

	return &quit, &errCh, wg
}

func TestMultipleBreaches(t *testing.T) {
	a := "multiple-breaches"
	u := api + "/breachedaccount/" + a + hibpTestDomain

	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		t.Error(err)
	}

	respCh, errCh := rChans()

	setUA(req)
	setAuthHeader(req)
	scheduleReq(req, &respCh, &errCh)

	resp := <-respCh
	err = <-errCh

	defer resp.Body.Close()

	if err != nil {
		t.Error(err)
	}

	t.Logf("%+v\n", resp)

	body, _ := io.ReadAll(resp.Body)
	b := string(body)

	t.Log(b)

	if apiKey == "" {
		switch {
		case resp.StatusCode != 200:
			switch {
			case resp.StatusCode == 401:
				t.Logf("apiKey is empty, a 401 is expected")
			case resp.StatusCode == 429:
				t.Log("skipping due to rate-limit")
				t.SkipNow()
			default:
				t.Errorf("apiKey is empty, expected 401, got: %q, body: %q", resp.Status, b)
			}

		default:
			t.Errorf("unexpected 200 without apiKey, got: %q, body: %q", resp.Status, b)
		}
	} else {
		if resp.StatusCode != 200 {
			if resp.StatusCode == 429 {
				t.Log("skipping due to rate-limit")
				t.SkipNow()
			} else {
				t.Errorf("expected 200, got: %q body: %q", resp.Status, b)
			}
		} else {
			want := `[{"Name":"Adobe"},{"Name":"Gawker"},{"Name":"Stratfor"}]`
			if want != b {
				t.Errorf("unexpected body, want: %q, got: %q", want, b)
			}
		}
	}

	account := a + hibpTestDomain
	if b, err := GetAllBreachesForAccount(account); err != nil {
		if errors.Is(err, ErrRateLimited) {
			t.Log("skipping due to rate-limit")
			t.SkipNow()
		} else {
			t.Errorf("error getting all breaches for account: %q", err)
		}
	} else {
		if b != nil {
			t.Logf("breach names for account %q: %#v", account, b)

			if len(b) != 1 {
				t.Errorf("expected 1 breach name for account %q, got: %d: %#v", account, len(b), b)
			} else {
				want := "Dubsmash"
				if b[0].Name != want {
					t.Errorf("expected breach name %q for account %q, got %q", want, account, b[0].Name)
				}
			}
		} else {
			t.Errorf("expected breach names for account %q, got nil", account)
		}
	}
}

func TestGetAllBreaches(t *testing.T) {
	_, err := GetAllBreaches()
	if err != nil {
		t.Errorf("error: %q", err)
	}
}

func TestGetAllBreachesForAccount(t *testing.T) {
	account := "dumb@test.com"

	if b, err := GetAllBreachesForAccount(account); err != nil {
		if errors.Is(err, ErrRateLimited) {
			t.Log("skipping due to rate-limit")
			t.SkipNow()
		} else {
			t.Errorf("error getting all breaches for account: %q", err)
		}
	} else {
		if b != nil {
			t.Logf("breach names for account %q: %#v", account, b)

			if len(b) != 1 {
				t.Errorf("expected 1 breach name for account %q, got: %d: %#v", account, len(b), b)
			} else {
				want := "Dubsmash"
				if b[0].Name != want {
					t.Errorf("expected breach name %q for account %q, got %q", want, account, b[0].Name)
				}
			}
		} else {
			t.Errorf("expected breach names for account %q, got nil", account)
		}
	}
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package localbreach

import (
	"path"
	"testing"
)

func TestLocalbreach(t *testing.T) {
	testDir := "./testdata"
	t1 := "test1.yaml"

	schema, err := Load(path.Join(testDir, t1))
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("schema: %+v", schema)
}

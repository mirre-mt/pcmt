// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package user

const (
	// AdminUname is the username of the initial administrator user.
	AdminUname = "admin"
	// AdminEmail is the email of the initial administrator user.
	AdminEmail = "admin@adminmail.admindomain"
)

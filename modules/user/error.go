// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package user

import "errors"

var (
	ErrUsersAlreadyPresent            = errors.New("don't call CreateFirst when there already are another users")
	ErrUserNotFound                   = errors.New("user not found")
	ErrFailedToQueryUser              = errors.New("failed to query user")
	ErrBadUUID                        = errors.New("invalid uuid")
	ErrPasswordEmpty                  = errors.New("password was empty")
	ErrNewPasswordCannotEqual         = errors.New("the new password cannot be the same as the old one")
	ErrUnfinishedSetupLastLoginUpdate = errors.New("not updating last_login for users with unfinished setup")
)

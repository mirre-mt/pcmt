// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"context"
	"testing"

	"git.dotya.ml/mirre-mt/pcmt/ent/enttest"
	"git.dotya.ml/mirre-mt/pcmt/slogging"
	_ "github.com/xiaoqidun/entps"
)

func TestUserExists(t *testing.T) {
	t.Parallel()

	connstr := "file:ent_tests?mode=memory&_fk=1"
	db := enttest.Open(t, "sqlite3", connstr)

	defer db.Close()

	if err := db.Schema.Create(context.Background()); err != nil {
		t.Errorf("failed to create schema resources: %v", err)
		t.FailNow()
	}

	username := "dude"
	email := "dude@b.cc"
	ctx := getCtx()

	usernameFound, err := UsernameExists(ctx, db, username)
	if err != nil {
		t.Errorf("error checking for username {%s} existence: %q",
			username,
			err,
		)
	}

	if usernameFound {
		t.Errorf("unexpected: user{%s} should not have been found",
			username,
		)
	}

	if _, err := EmailExists(ctx, db, email); err != nil {
		t.Errorf("unexpected: user email '%s' should not have been found",
			email,
		)
	}

	usr, err := CreateUser(ctx, db, email, username, "so strong")
	if err != nil {
		t.Fatalf("failed to create user, error: %q", err)
	} else if usr == nil {
		t.Fatal("got nil usr back")
	}

	if usr.Username != username {
		t.Errorf("got back wrong username, want: %s, got: %s", username, usr.Username)
	}

	usernameFound, err = UsernameExists(ctx, db, username)
	if err != nil {
		t.Errorf("error checking for username {%s} existence: %q",
			username,
			err,
		)
	}

	if !usernameFound {
		t.Errorf("unexpected: user{%s} should not have been found",
			username,
		)
	}

	exists, err := Exists(ctx, db, username, email)
	if err != nil {
		t.Errorf("error checking whether user exists: %q", err)
	}

	if !exists {
		t.Errorf("unexpected: user{%s} does not exists and they should", username)
	}
}

func getCtx() context.Context {
	l := slogging.Init(false)

	ctx := context.WithValue(
		context.Background(),
		CtxKey{},
		l,
	)

	return ctx
}

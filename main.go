// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"

	"git.dotya.ml/mirre-mt/pcmt/slogging"
)

func main() {
	err := run()
	if err != nil {
		l := slogging.Logger()
		if l == nil {
			l = slogging.Init(true)
		}

		l.Error("unrecoverable failure, stopping the app", "error", err)
		os.Exit(1)
	}
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package main

import "flag"

var (
	hostFlag           = flag.String("host", "unset", "host address to listen on")
	portFlag           = flag.Int("port", 0, "TCP port to listen on")
	configFlag         = flag.String("config", "config.dhall", "Default path of the config file")
	configIsPathFlag   = flag.Bool("configIsPath", true, "Whether the provided config is path or raw config")
	develFlag          = flag.Bool("devel", false, "Run the application in dev mode, connect to a local browser-sync instance for hot-reloading")
	licenseFlag        = flag.Bool("license", false, "Print licensing information and exit")
	versionFlag        = flag.Bool("version", false, "Print version and exit")
	importFlag         = flag.String("import", "", "Path to import breach data from")
	printMigrationFlag = flag.Bool("printMigration", false, "Print to stdout what is about to happen during the following database migration (mainly useful for debugging")
)

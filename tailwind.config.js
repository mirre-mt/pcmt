/**
 * Copyright 2023 wanderer <a_mirre at utb dot cz>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./templates/**.{tmpl,html}", "./templates/**/**.tmpl"],
  /* darkMode: 'class', */
  theme: {
    extend: {},
  },
  plugins: [],
}

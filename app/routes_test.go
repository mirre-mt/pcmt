// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package app

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	_ "github.com/xiaoqidun/entps"

	"git.dotya.ml/mirre-mt/pcmt/app/settings"
	"git.dotya.ml/mirre-mt/pcmt/config"
	"git.dotya.ml/mirre-mt/pcmt/ent/enttest"
	"git.dotya.ml/mirre-mt/pcmt/slogging"
)

const connstr = "file:ent_tests?mode=memory&_fk=1"

var conf = &config.Config{
	Port:      3005,
	LiveMode:  true,
	DevelMode: false,
	Session: struct {
		CookieName       string
		CookieAuthSecret string
		CookieEncrSecret string
		MaxAge           int
	}{
		CookieName:       "sessionz",
		CookieAuthSecret: "c50429df4c74c7d23652171460c1209d529815caf101c164e90a0ed20e6857411c43f721f0d7ab85842e7dc827475f451e85f104092ed69f3463d9498bb6f8a3",
		CookieEncrSecret: "5fa29053a2a6cd2b8aae686f768e6bd52c78de82fba64e94d3e0427ec3258320",
		MaxAge:           0,
	},
}

var (
	host  = new(string)
	port  = new(int)
	devel = new(bool)
)

func TestStaticRoute(t *testing.T) {
	setting := settings.New()
	a := &App{
		templatesPath: "../templates",
		assetsPath:    "../assets/public",
	}
	log := slogging.Init(false)
	tstRoute := "/static/"
	db := enttest.Open(t, "sqlite3", connstr)

	defer db.Close()

	if err := db.Schema.Create(context.Background()); err != nil {
		t.Fatalf("failed creating schema resources: %v", err)
	}

	*host = ""
	*port = 3500
	*devel = true

	setting.Consolidate(conf, host, port, devel, "test")

	err := a.Init(setting, log, db)
	if err != nil {
		t.Errorf("failed to initialise app: %v", a)
	}

	if err = a.SetupRoutes(); err != nil {
		t.Errorf("failed to set up routes: %q", err)
	}

	req := httptest.NewRequest(http.MethodGet, tstRoute, nil)
	rec := httptest.NewRecorder()

	a.E().ServeHTTP(rec, req)

	want := http.StatusMovedPermanently
	got := rec.Code

	if want != got {
		t.Fatalf("unexpected status code when testing %s, want: %d, got %d",
			tstRoute, want, got,
		)
	}

	uri, err := rec.Result().Location()
	if err != nil {
		t.Fatal("error getting response location")
	}

	if uri.Path != "/assets/" {
		t.Errorf("unexpected location, want: %s, got: %s", "/assets/", uri.Path)
	}
}

func BenchmarkStatic(b *testing.B) { //nolint:dupl
	setting := settings.New()
	a := &App{
		templatesPath: "../templates",
		assetsPath:    "../assets/public",
	}
	log := slogging.Init(false)
	db := enttest.Open(b, "sqlite3", connstr)

	defer db.Close()

	if err := db.Schema.Create(context.Background()); err != nil {
		b.Fatalf("failed creating schema resources: %v", err)
	}

	setting.Consolidate(conf, host, port, devel, "bench")

	err := a.Init(setting, log, db)
	if err != nil {
		b.Errorf("failed to initialise app: %v", a)
	}

	if err = a.SetupRoutes(); err != nil {
		b.Errorf("failed to set up routes: %q", err)
	}

	for i := 0; i < b.N; i++ {
		req := httptest.NewRequest(http.MethodGet, "/static/", nil)
		rec := httptest.NewRecorder()

		a.E().ServeHTTP(rec, req)
	}
}

func BenchmarkStatic2(b *testing.B) { //nolint:dupl
	setting := settings.New()
	a := &App{
		templatesPath: "../templates",
		assetsPath:    "../assets/public",
	}
	log := slogging.Init(false)
	db := enttest.Open(b, "sqlite3", connstr)

	defer db.Close()

	if err := db.Schema.Create(context.Background()); err != nil {
		b.Fatalf("failed creating schema resources: %v", err)
	}

	setting.Consolidate(conf, host, port, devel, "bench")

	err := a.Init(setting, log, db)
	if err != nil {
		b.Errorf("failed to initialise app: %v", a)
	}

	if err = a.SetupRoutes(); err != nil {
		b.Errorf("failed to set up routes: %q", err)
	}

	for i := 0; i < b.N; i++ {
		req := httptest.NewRequest(http.MethodGet, "/static2/", nil)
		rec := httptest.NewRecorder()

		a.E().ServeHTTP(rec, req)
	}
}

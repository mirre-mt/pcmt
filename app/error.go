// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package app

import "errors"

var (
	ErrAppSettingsUnset      = errors.New("somehow we got here with settings unset - contact the developer")
	ErrAppAlreadyInitialised = errors.New("app was already initialised")
)

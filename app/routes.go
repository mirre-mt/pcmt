// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package app

import (
	"net/http"

	"git.dotya.ml/mirre-mt/pcmt/handlers"
	modtmpl "git.dotya.ml/mirre-mt/pcmt/modules/template"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func (a *App) SetupRoutes() error {
	e := a.E()
	setting := a.setting
	assets := http.FileServer(a.getAssets())
	tmpls := a.getTemplates()

	modtmpl.Init(setting, tmpls)
	handlers.SetDBClient(a.db)
	// run this before declaring any handler funcs.
	handlers.InitHandlers(setting)

	e.Renderer = modtmpl.Renderer

	compress, err := handlers.WrapMiddlewareCompress()
	if err != nil {
		return err
	}

	xsrf := a.csrfConfig()

	// keep /static/* as a compatibility fallback for /assets.
	e.GET(
		"/static/*",
		func(c echo.Context) error {
			return c.Redirect(http.StatusMovedPermanently, c.Request().URL.Path)
		},
		middleware.Rewrite(
			map[string]string{"/static/*": "/assets/$1"},
		),
	)
	// alternative:
	// e.GET("/static/*", echo.WrapHandler(http.StripPrefix("/static/", assets)))
	e.GET("/assets/*", echo.WrapHandler(http.StripPrefix("/assets/", assets)), handlers.MiddlewareCache, compress)
	e.HEAD("/assets/*", echo.WrapHandler(http.StripPrefix("/assets/", assets)), handlers.MiddlewareCache, compress)

	e.GET("/healthz", handlers.Healthz())
	e.GET("/health", handlers.Healthz())

	base := e.Group("", xsrf)

	base.GET("/", handlers.Index(), compress)
	base.HEAD("/", handlers.Index())
	base.GET("/signin", handlers.Signin(), compress)
	base.POST("/signin", handlers.SigninPost(a.db))

	if a.setting.RegistrationAllowed {
		base.GET("/signup", handlers.Signup(), compress)
		base.POST("/signup", handlers.SignupPost(a.db))
	}

	base.GET("/home", handlers.Home(a.db))

	// handle weird attempts here.
	base.POST("/signin/*", func(c echo.Context) error {
		return c.NoContent(http.StatusNotFound)
	})
	base.POST("/signup/*", func(c echo.Context) error {
		return c.NoContent(http.StatusNotFound)
	})

	user := e.Group("/user", handlers.MiddlewareSession, xsrf)

	user.GET("/initial-password-change", handlers.InitialPasswordChange())
	user.POST("/initial-password-change", handlers.InitialPasswordChangePost())
	user.GET("/hibp-search", handlers.GetSearchHIBP())
	user.POST("/hibp-search", handlers.SearchHIBP())
	user.GET("/hibp-breach-details/:name", handlers.ViewHIBP())

	manage := e.Group("/manage", handlers.MiddlewareSession, xsrf)

	manage.GET("/api-keys", handlers.ManageAPIKeys(), compress)
	manage.GET("/users", handlers.ManageUsers(), compress)
	manage.GET("/users/new", handlers.ManageUsers(), compress)
	manage.POST("/users/create", handlers.CreateUser())
	manage.GET("/users/:id", handlers.ViewUser(), compress)
	manage.GET("/users/:id/edit", handlers.EditUser(), handlers.MiddlewareCache, compress)
	manage.GET("/users/:id/delete", handlers.DeleteUserConfirmation(), compress)
	manage.POST("/users/:id/update", handlers.UpdateUser())
	manage.POST("/users/:id/delete", handlers.DeleteUser())

	e.GET("/logout", handlers.Logout(), xsrf)
	e.POST("/logout", handlers.Logout(), handlers.MiddlewareSession, xsrf)

	return nil
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package app

import (
	"errors"
	"io/fs"
	"net/http"
	"os"
)

func (a *App) getAssets() http.FileSystem {
	live := a.setting.IsLive()

	if live {
		a.Logger().Info("assets loaded in live mode")

		_, err := os.ReadDir(a.assetsPath)
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				a.Logger().Error("assets dir does not exist")
			} else if errors.Is(err, os.ErrPermission) {
				a.Logger().Error("assets dir is not accessible, check permissions")
			}

			panic(err)
		}

		return http.FS(os.DirFS(a.assetsPath))
	}

	a.Logger().Info("assets loaded in embed mode")

	fsys, err := fs.Sub(a.embeds.assets, "assets/public")
	if err != nil {
		panic(err)
	}

	return http.FS(fsys)
}

func (a *App) getTemplates() fs.FS {
	live := a.setting.IsLive()

	if live {
		a.Logger().Info("templates loaded in live mode")

		_, err := os.ReadDir(a.templatesPath)
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				a.Logger().Errorf("templates dir '%s' does not exist", a.templatesPath)
			} else if errors.Is(err, os.ErrPermission) {
				a.Logger().Errorf("templates dir '%s' is not accessible, check permissions", a.templatesPath)
			}

			panic(err)
		}

		return os.DirFS(a.templatesPath)
	}

	a.Logger().Info("templates loaded in embed mode")

	fsys, err := fs.Sub(a.embeds.templates, ".")
	if err != nil {
		panic(err)
	}

	// alt:
	// func (a *App) getTemplates(live bool) http.FileSystem {
	// return http.FS(os.DirFS("../templates"))

	return fsys
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package settings

import (
	"flag"

	"git.dotya.ml/mirre-mt/pcmt/config"
)

// as per https://stackoverflow.com/a/54747682.
func isFlagPassed(name string) bool {
	found := false

	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})

	return found
}

// sortOutFlags checks whether any flag overrides were passed and their validity.
func (s *Settings) sortOutFlags(conf *config.Config, hostFlag *string, portFlag *int, develFlag *bool) {
	log.Debug("checking flag overrides")

	overrideMsg := "overriding '%s' based on a flag: %+v"

	if isFlagPassed("host") {
		if h := *hostFlag; h != "unset" && h != conf.Host {
			log.Debugf(overrideMsg, "host", h)
			s.SetHost(h)
		}
	}

	if isFlagPassed("port") {
		if p := *portFlag; p > 0 && p < 65536 {
			if p != conf.Port {
				log.Debugf(overrideMsg, "port", p)
				s.SetPort(p)
			}
		} else {
			log.Warnf("flag-supplied port '%d' outside of bounds, ignoring", p)
		}
	}

	if isFlagPassed("devel") {
		if d := *develFlag; d != conf.DevelMode {
			log.Debugf(overrideMsg, "develMode", d)
			s.SetIsDevel(d)

			log.Debug("making sure that CSP is set appropriately for devel mode (flag override)")
			s.SetHTTPCSP(conf.HTTP.ContentSecurityPolicy)
		}
	}
}

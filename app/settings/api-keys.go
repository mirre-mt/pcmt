// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package settings

import (
	"os"
)

func (s *Settings) setAPIKeys() {
	if hibpK := os.Getenv("PCMT_HIBP_API_KEY"); hibpK != "" {
		log.Info("setting HIBP API key from env var")
		s.SetAPIKeyHIBP(hibpK)
	}

	if dehashedK := os.Getenv("PCMT_DEHASHED_API_KEY"); dehashedK != "" {
		log.Info("setting dehashed.com API key from env var")
		s.SetAPIKeyDehashed(dehashedK)
	}
}

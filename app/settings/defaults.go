// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package settings

import "time"

const (
	defaultAppName                 = "pcmt"
	defaultPort                    = 3000
	defaultSessionMaxAge           = 86400 // seconds.
	defaultHTTPDomain              = "localhost"
	defaultCSP                     = "upgrade-insecure-requests; default-src 'self'; manifest-src 'self'; font-src 'self'; connect-src 'self'; script-src 'self'; style-src 'self'; object-src 'self'; frame-ancestors 'self'; base-uri 'self'; form-action 'self'"
	defaultCSPDevel                = "default-src 'self'; manifest-src 'self'; font-src 'self'; connect-src 'self' ws://localhost:3002 http://localhost:3002; script-src 'self' http://localhost:3002; style-src 'self'; object-src 'self'; frame-ancestors 'self'; base-uri 'self'; form-action 'self'"
	defaultServerWriteTimeout      = 30 * time.Second
	defaultServerReadHeaderTimeout = 30 * time.Second
	defaultLoggerSkipAssets        = true
)

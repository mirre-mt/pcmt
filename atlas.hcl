// Define an environment named "local"
env "local" {
  // Declare where the schema definition resides.
  // Also supported: ["file://multi.hcl", "file://schema.hcl"].
  src = "file://ent/migrate/migrations"

  // Define the URL of the database which is managed
  // in this environment.
  url = "postgres://postgres:postgres@localhost:5432/postgres?search_path=public&sslmode=disable"

  // Define the URL of the Dev Database for this environment
  // See: https://atlasgo.io/concepts/dev-database
  dev = "docker://postgres/15.3-alpine3.18/postgres?search_path=public"

  migration {
    // URL where the migration directory resides.
    dir = "file://ent/migrate/migrations"
    // An optional format of the migration directory:
    // atlas (default) | flyway | liquibase | goose | golang-migrate | dbmate
    format = "atlas"
  }
}

let ConfigSchema =
        https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
          sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50
      ? https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall

let Config = ConfigSchema.Schema

let c = Config::{ Host = False }

in  c

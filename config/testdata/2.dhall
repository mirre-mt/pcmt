let ConfigSchema =
        https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
          sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50
      ? https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall

let Config = ConfigSchema.Schema

let c =
      Config::{
      , Host = "localhost"
      , Session =
              ConfigSchema.Schema.default.Session
          //  { CookieAuthSecret =
                  "c50429df4c74c7d23652171460c1209d529815caf101c164e90a0ed20e6857411c43f721f0d7ab85842e7dc827475f451e85f104092ed69f3463d9498bb6f8a3"
              , CookieEncrSecret =
                  "5fa29053a2a6cd2b8aae686f768e6bd52c78de82fba64e94d3e0427ec3258320"
              }
      }

in  c

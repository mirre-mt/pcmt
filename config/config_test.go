// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"os"
	"testing"

	"git.dotya.ml/mirre-mt/pcmt/slogging"
)

func TestConfig(t *testing.T) {
	if os.Getenv("CI") == "true" {
		// the reason for skipping is initial normalisation can take a minute
		// and waiting that long for every test is not really feasible.
		t.Skip("we're running in CI apparently, skipping these tests")
	}

	confPath := "./testdata/"
	ts := []struct {
		name   string
		fails  bool
		isPath bool
		conf   string
	}{
		{
			// incorrect type of the `Host` field.
			name:   "1.dhall",
			fails:  true,
			isPath: true,
			conf:   "1.dhall",
		},
		{
			name:   "2.dhall",
			fails:  false,
			isPath: true,
			conf:   "2.dhall",
		},
		{
			name:   "3.dhall",
			fails:  false,
			isPath: false,
			conf: `
            let ConfigSchema =
                    https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
                      sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50
                  ? https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall

            let Config = ConfigSchema.Schema

            let c =
                  Config::{
                  , DevelMode = True
                  , Session =
                          ConfigSchema.Schema.default.Session
                      //  { CookieAuthSecret =
                              "c50429df4c74c7d23652171460c1209d529815caf101c164e90a0ed20e6857411c43f721f0d7ab85842e7dc827475f451e85f104092ed69f3463d9498bb6f8a3"
                          , CookieEncrSecret =
                              "5fa29053a2a6cd2b8aae686f768e6bd52c78de82fba64e94d3e0427ec3258320"
                          }
                  }

            in  c
			`,
		},
		{
			// misses final `in`.
			name:   "4.dhall",
			fails:  true,
			isPath: false,
			conf: `
			let ConfigSchema =
				  https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
					sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50
				  ? https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall

			let Config = ConfigSchema.Schema

			let c = Config::{ DevelMode = True }
			`,
		},
		{
			name:   "5.dhall",
			fails:  false,
			isPath: false,
			conf: `
			let ConfigSchema =
				  https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
				  sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50
			in ConfigSchema.Schema::{
			   , DevelMode = True
			   , Port = 5555
			   , Session =
			           ConfigSchema.Schema.default.Session
			       //  { CookieAuthSecret =
			               "c50429df4c74c7d23652171460c1209d529815caf101c164e90a0ed20e6857411c43f721f0d7ab85842e7dc827475f451e85f104092ed69f3463d9498bb6f8a3"
			             , CookieEncrSecret =
			               "5fa29053a2a6cd2b8aae686f768e6bd52c78de82fba64e94d3e0427ec3258320"
			           }
			   }
			`,
		},
		{
			name:   "6.dhall",
			fails:  false,
			isPath: false,
			conf: `
            let ConfigSchema =
                  https://git.dotya.ml/mirre-mt/pcmt-config-schema/raw/tag/0.0.1-rc.2/package.dhall
                    sha256:9082079ea4d41cc290c879a6a7e2034a2914949c30c337975cc5c6fecfc0da50

            in      ConfigSchema.Schema.default
                //  { DevelMode = True
                    , Session =
                            ConfigSchema.Schema.default.Session
                        //  { CookieAuthSecret =
                                "c50429df4c74c7d23652171460c1209d529815caf101c164e90a0ed20e6857411c43f721f0d7ab85842e7dc827475f451e85f104092ed69f3463d9498bb6f8a3"
                            , CookieEncrSecret =
                                "5fa29053a2a6cd2b8aae686f768e6bd52c78de82fba64e94d3e0427ec3258320"
                            }
                    }
			`,
		},
	}

	_ = slogging.Init(false)

	for _, tc := range ts {
		t.Logf("running test case %s", tc.name)

		var err error

		shouldFail := tc.fails

		if tc.isPath {
			_, err = Load(confPath+tc.conf, tc.isPath)
		} else {
			_, err = Load(tc.conf, tc.isPath)
		}

		switch {
		case err == nil && shouldFail:
			t.Errorf("test case '%s' should have failed", tc.name)

		case err != nil && !shouldFail:
			t.Log(err)
			t.Errorf("test case '%s' should not have failed", tc.name)
		}
	}
}

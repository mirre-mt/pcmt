/**
 * Copyright 2023 wanderer <a_mirre at utb dot cz>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

module.exports = {
  multipass: true,
  plugins: [
    {
      name: 'preset-default',
      params: {
        overrides: {
          removeViewBox: false
        }
      }
    },
    'removeStyleElement',
    'removeScriptElement',
    {
      name: 'removeAttrs',
      params: {
        attrs: ['xmlns', 'id', 'class', 'data-name', 'fill', 'transform', 'href', 'clip-path', 'clip-rule']
      }
    },
    {
      name: 'addClassesToSVGElement',
      params: {classNames: ['w-6 h-6 mx-3 text-gray-300 dark:text-gray-500 fill-none stroke-current stroke-2']}
    },
  ]
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package main

import "errors"

var (
	errDBNotConfigured     = errors.New("Database connection string or database type were not set")
	errUnsupportedDBType   = errors.New("DBTYPE can only be one of postgres or sqlite3")
	errDBConnFailed        = errors.New("Failed to open a connection to the database")
	errImportFailed        = errors.New("Import of local breach data failed")
	errHIBPSchedulerFailed = errors.New("HIBP requests scheduler failed")
)

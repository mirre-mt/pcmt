// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package main

import "embed"

var (
	//go:embed all:templates
	templates embed.FS

	//go:embed all:assets/public
	assets embed.FS
)

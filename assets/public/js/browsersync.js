(function() {
    try {
        var script = document.createElement('script');
        if ('async') {
            script.async = true;
        }
        // script.src = 'http://HOST:3002/browser-sync/browser-sync-client.js?v=2.29.0'.replace("HOST", location.hostname);
        script.src = 'http://localhost:3002/browser-sync/browser-sync-client.js?v=2.29.0';
        if (document.body) {
            document.body.appendChild(script);
        }
    } catch (e) {
        console.error("Browsersync: could not append script tag", e);
    }
})()

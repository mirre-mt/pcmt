// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"flag"
	"testing"
)

func TestPrintLicense(t *testing.T) {
	t.Parallel()
	t.Log("print license and exit")

	if err := flag.Set("license", "true"); err != nil {
		t.Fatal("failed to set license flag")
	}

	err := run()
	if err != nil {
		t.Fatal(err)
	}
}

func TestPrintHeader(t *testing.T) {
	t.Parallel()
	printHeader()
}

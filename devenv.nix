{ pkgs, ... }:

{
  env.CGO_ENABLED = 0;
  env.PCMT_DBTYPE = "postgres";
  env.PCMT_CONNSTRING = "host=127.0.0.1 sslmode=disable port=5432 user=postgres dbname=postgres password=postgres";
  env.PCMT_SESSION_AUTH_SECRET = "c43fb09c24a35f71a37e755f031b31aa733a44e6d07a4d7086c0aefe080ad6817d92ffe8cc12df0fce67938c6f42ffa0a6cefdbfb1d23b04d32ee4e2615ae118";
  env.PCMT_SESSION_ENCR_SECRET = "a222d20971aafde2aa5cb4d52c31f5761a0649e0d308a0713ad88022805b92db";

  packages = with pkgs; [ 
    git 
    go_1_20 
    dhall 
    dhall-json 

    nodePackages.npm
    nodePackages.tailwindcss
    nodePackages.browser-sync

    podman
    just
    air
  ];

  enterShell = ''
    echo -- welcome to devenv shell --
  '';

  # https://devenv.sh/processes/
  processes = {
    pcmt = {
      exec = "air";
    };
    brs = {
      exec = "npm i; npm run watch-brs";
    };
    db = {
      exec = "just dbstart";
    };
  };
}

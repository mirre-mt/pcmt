// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package main

//go:generate npm i
//go:generate npx tailwindcss -i ./assets/input/css/tailwind.css -o ./assets/public/css/pcmt.css --minify

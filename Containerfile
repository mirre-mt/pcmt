# syntax=docker/dockerfile-upstream:master-labs
# Copyright 2023 wanderer <a_mirre at utb dot cz>
# SPDX-License-Identifier: AGPL-3.0-only

FROM docker.io/library/alpine:3.18.0 as dhall-cache
# https://stackoverflow.com/questions/53681522/share-variable-in-multi-stage-dockerfile-arg-before-from-not-substituted
# https://docs.docker.com/engine/reference/builder/#arg
ARG VERSION
ARG BUILD_DATE
ARG VCS_REF
ENV XDG_CACHE_HOME=/var/cache
ENV DHALL_VERSION=1.42.0

ADD https://git.dotya.ml/mirre-mt/pcmt/raw/branch/development/exampleConfig.dhall /tmp
ADD https://github.com/dhall-lang/dhall-haskell/releases/download/${DHALL_VERSION}/dhall-${DHALL_VERSION}-x86_64-linux.tar.bz2 /tmp/dhall.tar.bz2

WORKDIR /tmp

RUN tar xf /tmp/dhall.tar.bz2 \
    && mv /tmp/bin/dhall /usr/bin \
    && echo "Normalise exampleConfig.dhall (saving the result in cache)" \
    && time dhall --file /tmp/exampleConfig.dhall


FROM docker.io/library/golang:1.20.6-alpine3.18 as go-build
ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

COPY . /go/pcmt

WORKDIR /go/pcmt

RUN apk add --no-cache npm=9.6.6-r0 \
    && go generate -v . \
    && CGO_ENABLED=0 \
    go build -v -trimpath -ldflags="-s -w -X main.version=${VERSION:-prod}" .


FROM docker.io/immawanderer/scratch-cacerts:linux-amd64
ARG BUILD_DATE
ARG VCS_REF
ENV XDG_CACHE_HOME=/root/.cache

COPY --from=dhall-cache /var/cache/dhall-haskell /root/.cache/dhall-haskell
COPY --from=dhall-cache /var/cache/dhall         /root/.cache/dhall
COPY --from=dhall-cache /tmp/exampleConfig.dhall /etc/pcmt/config.dhall
COPY --from=go-build    /go/pcmt/pcmt            /bin/pcmt

LABEL description="Password Compromise Monitoring Tool" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/mirre-mt/pcmt.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=AGPL-3.0-only

ENTRYPOINT ["/bin/pcmt"]
CMD ["-help"]

#  vim: set ts=4 ft=dockerfile fenc=utf-8 ff=unix :

// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/dialect/sql/sqljson"
	"entgo.io/ent/schema/field"
	"git.dotya.ml/mirre-mt/pcmt/ent/hibp"
	"git.dotya.ml/mirre-mt/pcmt/ent/predicate"
	"git.dotya.ml/mirre-mt/pcmt/ent/trackedbreaches"
	"github.com/google/uuid"
)

// HIBPUpdate is the builder for updating HIBP entities.
type HIBPUpdate struct {
	config
	hooks    []Hook
	mutation *HIBPMutation
}

// Where appends a list predicates to the HIBPUpdate builder.
func (hu *HIBPUpdate) Where(ps ...predicate.HIBP) *HIBPUpdate {
	hu.mutation.Where(ps...)
	return hu
}

// SetTitle sets the "title" field.
func (hu *HIBPUpdate) SetTitle(s string) *HIBPUpdate {
	hu.mutation.SetTitle(s)
	return hu
}

// SetBreachDate sets the "breach_date" field.
func (hu *HIBPUpdate) SetBreachDate(s string) *HIBPUpdate {
	hu.mutation.SetBreachDate(s)
	return hu
}

// SetAddedDate sets the "added_date" field.
func (hu *HIBPUpdate) SetAddedDate(t time.Time) *HIBPUpdate {
	hu.mutation.SetAddedDate(t)
	return hu
}

// SetModifiedDate sets the "modified_date" field.
func (hu *HIBPUpdate) SetModifiedDate(t time.Time) *HIBPUpdate {
	hu.mutation.SetModifiedDate(t)
	return hu
}

// SetPwnCount sets the "pwn_count" field.
func (hu *HIBPUpdate) SetPwnCount(i int) *HIBPUpdate {
	hu.mutation.ResetPwnCount()
	hu.mutation.SetPwnCount(i)
	return hu
}

// AddPwnCount adds i to the "pwn_count" field.
func (hu *HIBPUpdate) AddPwnCount(i int) *HIBPUpdate {
	hu.mutation.AddPwnCount(i)
	return hu
}

// SetDescription sets the "description" field.
func (hu *HIBPUpdate) SetDescription(s string) *HIBPUpdate {
	hu.mutation.SetDescription(s)
	return hu
}

// SetNillableDescription sets the "description" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableDescription(s *string) *HIBPUpdate {
	if s != nil {
		hu.SetDescription(*s)
	}
	return hu
}

// ClearDescription clears the value of the "description" field.
func (hu *HIBPUpdate) ClearDescription() *HIBPUpdate {
	hu.mutation.ClearDescription()
	return hu
}

// SetDataclasses sets the "dataclasses" field.
func (hu *HIBPUpdate) SetDataclasses(s []string) *HIBPUpdate {
	hu.mutation.SetDataclasses(s)
	return hu
}

// AppendDataclasses appends s to the "dataclasses" field.
func (hu *HIBPUpdate) AppendDataclasses(s []string) *HIBPUpdate {
	hu.mutation.AppendDataclasses(s)
	return hu
}

// SetIsVerified sets the "is_verified" field.
func (hu *HIBPUpdate) SetIsVerified(b bool) *HIBPUpdate {
	hu.mutation.SetIsVerified(b)
	return hu
}

// SetNillableIsVerified sets the "is_verified" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableIsVerified(b *bool) *HIBPUpdate {
	if b != nil {
		hu.SetIsVerified(*b)
	}
	return hu
}

// SetIsFabricated sets the "is_fabricated" field.
func (hu *HIBPUpdate) SetIsFabricated(b bool) *HIBPUpdate {
	hu.mutation.SetIsFabricated(b)
	return hu
}

// SetNillableIsFabricated sets the "is_fabricated" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableIsFabricated(b *bool) *HIBPUpdate {
	if b != nil {
		hu.SetIsFabricated(*b)
	}
	return hu
}

// SetIsSensitive sets the "is_sensitive" field.
func (hu *HIBPUpdate) SetIsSensitive(b bool) *HIBPUpdate {
	hu.mutation.SetIsSensitive(b)
	return hu
}

// SetNillableIsSensitive sets the "is_sensitive" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableIsSensitive(b *bool) *HIBPUpdate {
	if b != nil {
		hu.SetIsSensitive(*b)
	}
	return hu
}

// SetIsRetired sets the "is_retired" field.
func (hu *HIBPUpdate) SetIsRetired(b bool) *HIBPUpdate {
	hu.mutation.SetIsRetired(b)
	return hu
}

// SetNillableIsRetired sets the "is_retired" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableIsRetired(b *bool) *HIBPUpdate {
	if b != nil {
		hu.SetIsRetired(*b)
	}
	return hu
}

// SetIsSpamList sets the "is_spamList" field.
func (hu *HIBPUpdate) SetIsSpamList(b bool) *HIBPUpdate {
	hu.mutation.SetIsSpamList(b)
	return hu
}

// SetNillableIsSpamList sets the "is_spamList" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableIsSpamList(b *bool) *HIBPUpdate {
	if b != nil {
		hu.SetIsSpamList(*b)
	}
	return hu
}

// SetIsMalware sets the "is_malware" field.
func (hu *HIBPUpdate) SetIsMalware(b bool) *HIBPUpdate {
	hu.mutation.SetIsMalware(b)
	return hu
}

// SetNillableIsMalware sets the "is_malware" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableIsMalware(b *bool) *HIBPUpdate {
	if b != nil {
		hu.SetIsMalware(*b)
	}
	return hu
}

// SetLogoPath sets the "logo_path" field.
func (hu *HIBPUpdate) SetLogoPath(s string) *HIBPUpdate {
	hu.mutation.SetLogoPath(s)
	return hu
}

// SetNillableLogoPath sets the "logo_path" field if the given value is not nil.
func (hu *HIBPUpdate) SetNillableLogoPath(s *string) *HIBPUpdate {
	if s != nil {
		hu.SetLogoPath(*s)
	}
	return hu
}

// ClearLogoPath clears the value of the "logo_path" field.
func (hu *HIBPUpdate) ClearLogoPath() *HIBPUpdate {
	hu.mutation.ClearLogoPath()
	return hu
}

// SetTrackedBreachesID sets the "tracked_breaches" edge to the TrackedBreaches entity by ID.
func (hu *HIBPUpdate) SetTrackedBreachesID(id uuid.UUID) *HIBPUpdate {
	hu.mutation.SetTrackedBreachesID(id)
	return hu
}

// SetNillableTrackedBreachesID sets the "tracked_breaches" edge to the TrackedBreaches entity by ID if the given value is not nil.
func (hu *HIBPUpdate) SetNillableTrackedBreachesID(id *uuid.UUID) *HIBPUpdate {
	if id != nil {
		hu = hu.SetTrackedBreachesID(*id)
	}
	return hu
}

// SetTrackedBreaches sets the "tracked_breaches" edge to the TrackedBreaches entity.
func (hu *HIBPUpdate) SetTrackedBreaches(t *TrackedBreaches) *HIBPUpdate {
	return hu.SetTrackedBreachesID(t.ID)
}

// Mutation returns the HIBPMutation object of the builder.
func (hu *HIBPUpdate) Mutation() *HIBPMutation {
	return hu.mutation
}

// ClearTrackedBreaches clears the "tracked_breaches" edge to the TrackedBreaches entity.
func (hu *HIBPUpdate) ClearTrackedBreaches() *HIBPUpdate {
	hu.mutation.ClearTrackedBreaches()
	return hu
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (hu *HIBPUpdate) Save(ctx context.Context) (int, error) {
	return withHooks(ctx, hu.sqlSave, hu.mutation, hu.hooks)
}

// SaveX is like Save, but panics if an error occurs.
func (hu *HIBPUpdate) SaveX(ctx context.Context) int {
	affected, err := hu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (hu *HIBPUpdate) Exec(ctx context.Context) error {
	_, err := hu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (hu *HIBPUpdate) ExecX(ctx context.Context) {
	if err := hu.Exec(ctx); err != nil {
		panic(err)
	}
}

func (hu *HIBPUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := sqlgraph.NewUpdateSpec(hibp.Table, hibp.Columns, sqlgraph.NewFieldSpec(hibp.FieldID, field.TypeUUID))
	if ps := hu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := hu.mutation.Title(); ok {
		_spec.SetField(hibp.FieldTitle, field.TypeString, value)
	}
	if value, ok := hu.mutation.BreachDate(); ok {
		_spec.SetField(hibp.FieldBreachDate, field.TypeString, value)
	}
	if value, ok := hu.mutation.AddedDate(); ok {
		_spec.SetField(hibp.FieldAddedDate, field.TypeTime, value)
	}
	if value, ok := hu.mutation.ModifiedDate(); ok {
		_spec.SetField(hibp.FieldModifiedDate, field.TypeTime, value)
	}
	if value, ok := hu.mutation.PwnCount(); ok {
		_spec.SetField(hibp.FieldPwnCount, field.TypeInt, value)
	}
	if value, ok := hu.mutation.AddedPwnCount(); ok {
		_spec.AddField(hibp.FieldPwnCount, field.TypeInt, value)
	}
	if value, ok := hu.mutation.Description(); ok {
		_spec.SetField(hibp.FieldDescription, field.TypeString, value)
	}
	if hu.mutation.DescriptionCleared() {
		_spec.ClearField(hibp.FieldDescription, field.TypeString)
	}
	if value, ok := hu.mutation.Dataclasses(); ok {
		_spec.SetField(hibp.FieldDataclasses, field.TypeJSON, value)
	}
	if value, ok := hu.mutation.AppendedDataclasses(); ok {
		_spec.AddModifier(func(u *sql.UpdateBuilder) {
			sqljson.Append(u, hibp.FieldDataclasses, value)
		})
	}
	if value, ok := hu.mutation.IsVerified(); ok {
		_spec.SetField(hibp.FieldIsVerified, field.TypeBool, value)
	}
	if value, ok := hu.mutation.IsFabricated(); ok {
		_spec.SetField(hibp.FieldIsFabricated, field.TypeBool, value)
	}
	if value, ok := hu.mutation.IsSensitive(); ok {
		_spec.SetField(hibp.FieldIsSensitive, field.TypeBool, value)
	}
	if value, ok := hu.mutation.IsRetired(); ok {
		_spec.SetField(hibp.FieldIsRetired, field.TypeBool, value)
	}
	if value, ok := hu.mutation.IsSpamList(); ok {
		_spec.SetField(hibp.FieldIsSpamList, field.TypeBool, value)
	}
	if value, ok := hu.mutation.IsMalware(); ok {
		_spec.SetField(hibp.FieldIsMalware, field.TypeBool, value)
	}
	if value, ok := hu.mutation.LogoPath(); ok {
		_spec.SetField(hibp.FieldLogoPath, field.TypeString, value)
	}
	if hu.mutation.LogoPathCleared() {
		_spec.ClearField(hibp.FieldLogoPath, field.TypeString)
	}
	if hu.mutation.TrackedBreachesCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   hibp.TrackedBreachesTable,
			Columns: []string{hibp.TrackedBreachesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(trackedbreaches.FieldID, field.TypeUUID),
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := hu.mutation.TrackedBreachesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   hibp.TrackedBreachesTable,
			Columns: []string{hibp.TrackedBreachesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(trackedbreaches.FieldID, field.TypeUUID),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, hu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{hibp.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return 0, err
	}
	hu.mutation.done = true
	return n, nil
}

// HIBPUpdateOne is the builder for updating a single HIBP entity.
type HIBPUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *HIBPMutation
}

// SetTitle sets the "title" field.
func (huo *HIBPUpdateOne) SetTitle(s string) *HIBPUpdateOne {
	huo.mutation.SetTitle(s)
	return huo
}

// SetBreachDate sets the "breach_date" field.
func (huo *HIBPUpdateOne) SetBreachDate(s string) *HIBPUpdateOne {
	huo.mutation.SetBreachDate(s)
	return huo
}

// SetAddedDate sets the "added_date" field.
func (huo *HIBPUpdateOne) SetAddedDate(t time.Time) *HIBPUpdateOne {
	huo.mutation.SetAddedDate(t)
	return huo
}

// SetModifiedDate sets the "modified_date" field.
func (huo *HIBPUpdateOne) SetModifiedDate(t time.Time) *HIBPUpdateOne {
	huo.mutation.SetModifiedDate(t)
	return huo
}

// SetPwnCount sets the "pwn_count" field.
func (huo *HIBPUpdateOne) SetPwnCount(i int) *HIBPUpdateOne {
	huo.mutation.ResetPwnCount()
	huo.mutation.SetPwnCount(i)
	return huo
}

// AddPwnCount adds i to the "pwn_count" field.
func (huo *HIBPUpdateOne) AddPwnCount(i int) *HIBPUpdateOne {
	huo.mutation.AddPwnCount(i)
	return huo
}

// SetDescription sets the "description" field.
func (huo *HIBPUpdateOne) SetDescription(s string) *HIBPUpdateOne {
	huo.mutation.SetDescription(s)
	return huo
}

// SetNillableDescription sets the "description" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableDescription(s *string) *HIBPUpdateOne {
	if s != nil {
		huo.SetDescription(*s)
	}
	return huo
}

// ClearDescription clears the value of the "description" field.
func (huo *HIBPUpdateOne) ClearDescription() *HIBPUpdateOne {
	huo.mutation.ClearDescription()
	return huo
}

// SetDataclasses sets the "dataclasses" field.
func (huo *HIBPUpdateOne) SetDataclasses(s []string) *HIBPUpdateOne {
	huo.mutation.SetDataclasses(s)
	return huo
}

// AppendDataclasses appends s to the "dataclasses" field.
func (huo *HIBPUpdateOne) AppendDataclasses(s []string) *HIBPUpdateOne {
	huo.mutation.AppendDataclasses(s)
	return huo
}

// SetIsVerified sets the "is_verified" field.
func (huo *HIBPUpdateOne) SetIsVerified(b bool) *HIBPUpdateOne {
	huo.mutation.SetIsVerified(b)
	return huo
}

// SetNillableIsVerified sets the "is_verified" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableIsVerified(b *bool) *HIBPUpdateOne {
	if b != nil {
		huo.SetIsVerified(*b)
	}
	return huo
}

// SetIsFabricated sets the "is_fabricated" field.
func (huo *HIBPUpdateOne) SetIsFabricated(b bool) *HIBPUpdateOne {
	huo.mutation.SetIsFabricated(b)
	return huo
}

// SetNillableIsFabricated sets the "is_fabricated" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableIsFabricated(b *bool) *HIBPUpdateOne {
	if b != nil {
		huo.SetIsFabricated(*b)
	}
	return huo
}

// SetIsSensitive sets the "is_sensitive" field.
func (huo *HIBPUpdateOne) SetIsSensitive(b bool) *HIBPUpdateOne {
	huo.mutation.SetIsSensitive(b)
	return huo
}

// SetNillableIsSensitive sets the "is_sensitive" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableIsSensitive(b *bool) *HIBPUpdateOne {
	if b != nil {
		huo.SetIsSensitive(*b)
	}
	return huo
}

// SetIsRetired sets the "is_retired" field.
func (huo *HIBPUpdateOne) SetIsRetired(b bool) *HIBPUpdateOne {
	huo.mutation.SetIsRetired(b)
	return huo
}

// SetNillableIsRetired sets the "is_retired" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableIsRetired(b *bool) *HIBPUpdateOne {
	if b != nil {
		huo.SetIsRetired(*b)
	}
	return huo
}

// SetIsSpamList sets the "is_spamList" field.
func (huo *HIBPUpdateOne) SetIsSpamList(b bool) *HIBPUpdateOne {
	huo.mutation.SetIsSpamList(b)
	return huo
}

// SetNillableIsSpamList sets the "is_spamList" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableIsSpamList(b *bool) *HIBPUpdateOne {
	if b != nil {
		huo.SetIsSpamList(*b)
	}
	return huo
}

// SetIsMalware sets the "is_malware" field.
func (huo *HIBPUpdateOne) SetIsMalware(b bool) *HIBPUpdateOne {
	huo.mutation.SetIsMalware(b)
	return huo
}

// SetNillableIsMalware sets the "is_malware" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableIsMalware(b *bool) *HIBPUpdateOne {
	if b != nil {
		huo.SetIsMalware(*b)
	}
	return huo
}

// SetLogoPath sets the "logo_path" field.
func (huo *HIBPUpdateOne) SetLogoPath(s string) *HIBPUpdateOne {
	huo.mutation.SetLogoPath(s)
	return huo
}

// SetNillableLogoPath sets the "logo_path" field if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableLogoPath(s *string) *HIBPUpdateOne {
	if s != nil {
		huo.SetLogoPath(*s)
	}
	return huo
}

// ClearLogoPath clears the value of the "logo_path" field.
func (huo *HIBPUpdateOne) ClearLogoPath() *HIBPUpdateOne {
	huo.mutation.ClearLogoPath()
	return huo
}

// SetTrackedBreachesID sets the "tracked_breaches" edge to the TrackedBreaches entity by ID.
func (huo *HIBPUpdateOne) SetTrackedBreachesID(id uuid.UUID) *HIBPUpdateOne {
	huo.mutation.SetTrackedBreachesID(id)
	return huo
}

// SetNillableTrackedBreachesID sets the "tracked_breaches" edge to the TrackedBreaches entity by ID if the given value is not nil.
func (huo *HIBPUpdateOne) SetNillableTrackedBreachesID(id *uuid.UUID) *HIBPUpdateOne {
	if id != nil {
		huo = huo.SetTrackedBreachesID(*id)
	}
	return huo
}

// SetTrackedBreaches sets the "tracked_breaches" edge to the TrackedBreaches entity.
func (huo *HIBPUpdateOne) SetTrackedBreaches(t *TrackedBreaches) *HIBPUpdateOne {
	return huo.SetTrackedBreachesID(t.ID)
}

// Mutation returns the HIBPMutation object of the builder.
func (huo *HIBPUpdateOne) Mutation() *HIBPMutation {
	return huo.mutation
}

// ClearTrackedBreaches clears the "tracked_breaches" edge to the TrackedBreaches entity.
func (huo *HIBPUpdateOne) ClearTrackedBreaches() *HIBPUpdateOne {
	huo.mutation.ClearTrackedBreaches()
	return huo
}

// Where appends a list predicates to the HIBPUpdate builder.
func (huo *HIBPUpdateOne) Where(ps ...predicate.HIBP) *HIBPUpdateOne {
	huo.mutation.Where(ps...)
	return huo
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (huo *HIBPUpdateOne) Select(field string, fields ...string) *HIBPUpdateOne {
	huo.fields = append([]string{field}, fields...)
	return huo
}

// Save executes the query and returns the updated HIBP entity.
func (huo *HIBPUpdateOne) Save(ctx context.Context) (*HIBP, error) {
	return withHooks(ctx, huo.sqlSave, huo.mutation, huo.hooks)
}

// SaveX is like Save, but panics if an error occurs.
func (huo *HIBPUpdateOne) SaveX(ctx context.Context) *HIBP {
	node, err := huo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (huo *HIBPUpdateOne) Exec(ctx context.Context) error {
	_, err := huo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (huo *HIBPUpdateOne) ExecX(ctx context.Context) {
	if err := huo.Exec(ctx); err != nil {
		panic(err)
	}
}

func (huo *HIBPUpdateOne) sqlSave(ctx context.Context) (_node *HIBP, err error) {
	_spec := sqlgraph.NewUpdateSpec(hibp.Table, hibp.Columns, sqlgraph.NewFieldSpec(hibp.FieldID, field.TypeUUID))
	id, ok := huo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "HIBP.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := huo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, hibp.FieldID)
		for _, f := range fields {
			if !hibp.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != hibp.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := huo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := huo.mutation.Title(); ok {
		_spec.SetField(hibp.FieldTitle, field.TypeString, value)
	}
	if value, ok := huo.mutation.BreachDate(); ok {
		_spec.SetField(hibp.FieldBreachDate, field.TypeString, value)
	}
	if value, ok := huo.mutation.AddedDate(); ok {
		_spec.SetField(hibp.FieldAddedDate, field.TypeTime, value)
	}
	if value, ok := huo.mutation.ModifiedDate(); ok {
		_spec.SetField(hibp.FieldModifiedDate, field.TypeTime, value)
	}
	if value, ok := huo.mutation.PwnCount(); ok {
		_spec.SetField(hibp.FieldPwnCount, field.TypeInt, value)
	}
	if value, ok := huo.mutation.AddedPwnCount(); ok {
		_spec.AddField(hibp.FieldPwnCount, field.TypeInt, value)
	}
	if value, ok := huo.mutation.Description(); ok {
		_spec.SetField(hibp.FieldDescription, field.TypeString, value)
	}
	if huo.mutation.DescriptionCleared() {
		_spec.ClearField(hibp.FieldDescription, field.TypeString)
	}
	if value, ok := huo.mutation.Dataclasses(); ok {
		_spec.SetField(hibp.FieldDataclasses, field.TypeJSON, value)
	}
	if value, ok := huo.mutation.AppendedDataclasses(); ok {
		_spec.AddModifier(func(u *sql.UpdateBuilder) {
			sqljson.Append(u, hibp.FieldDataclasses, value)
		})
	}
	if value, ok := huo.mutation.IsVerified(); ok {
		_spec.SetField(hibp.FieldIsVerified, field.TypeBool, value)
	}
	if value, ok := huo.mutation.IsFabricated(); ok {
		_spec.SetField(hibp.FieldIsFabricated, field.TypeBool, value)
	}
	if value, ok := huo.mutation.IsSensitive(); ok {
		_spec.SetField(hibp.FieldIsSensitive, field.TypeBool, value)
	}
	if value, ok := huo.mutation.IsRetired(); ok {
		_spec.SetField(hibp.FieldIsRetired, field.TypeBool, value)
	}
	if value, ok := huo.mutation.IsSpamList(); ok {
		_spec.SetField(hibp.FieldIsSpamList, field.TypeBool, value)
	}
	if value, ok := huo.mutation.IsMalware(); ok {
		_spec.SetField(hibp.FieldIsMalware, field.TypeBool, value)
	}
	if value, ok := huo.mutation.LogoPath(); ok {
		_spec.SetField(hibp.FieldLogoPath, field.TypeString, value)
	}
	if huo.mutation.LogoPathCleared() {
		_spec.ClearField(hibp.FieldLogoPath, field.TypeString)
	}
	if huo.mutation.TrackedBreachesCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   hibp.TrackedBreachesTable,
			Columns: []string{hibp.TrackedBreachesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(trackedbreaches.FieldID, field.TypeUUID),
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := huo.mutation.TrackedBreachesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   hibp.TrackedBreachesTable,
			Columns: []string{hibp.TrackedBreachesColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(trackedbreaches.FieldID, field.TypeUUID),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	_node = &HIBP{config: huo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, huo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{hibp.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	huo.mutation.done = true
	return _node, nil
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package ent

//go:generate go run -mod=mod entgo.io/ent/cmd/ent generate ./schema

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// AgeKey holds the schema definition for the AgeKey entity.
type AgeKey struct {
	ent.Schema
}

// Fields of the AgeKey.
func (AgeKey) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		field.Bytes("key").
			Nillable().
			NotEmpty().
			Comment("age ed25519 identity stored encrypted using AES256"),
		field.Time("created").
			Default(time.Now).
			Immutable(),
		field.Time("updated").
			Default(time.Now),
		field.UUID("owner", uuid.UUID{}).
			Comment("id of the key owner").
			// every user should only ever have one key, so no id should ever
			// be used twice in this column.
			Unique().
			// the owner of the key never changes.
			Immutable(),
	}
}

// Edges of the AgeKey.
func (AgeKey) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).
			Ref("agekey").
			Field("owner").
			Unique().
			Immutable().
			Required(),
	}
}

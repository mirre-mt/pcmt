// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// TrackedBreaches holds the schema definition for the TrackedBreaches entity.
type TrackedBreaches struct {
	ent.Schema
}

// Fields of the TrackedBreaches.
func (TrackedBreaches) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		field.UUID("breach", uuid.UUID{}),
		field.Bool("online").
			Default(true).
			Immutable().
			Comment("Whether the breach is a locally added one or obtained from an online service"),
		field.UUID("owner", uuid.UUID{}).
			Comment("id of the user tracking the breach").
			Immutable(),
	}
}

// Edges of the TrackedBreaches.
func (TrackedBreaches) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).
			Ref("tracked_breaches").
			Field("owner").
			Unique().
			Immutable().
			Required(),
		edge.To("localbreach", LocalBreach.Type),
		edge.To("hibp", HIBP.Type),
	}
}

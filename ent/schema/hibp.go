// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// HIBP holds the schema definition for the HIBP entity.
type HIBP struct {
	ent.Schema
}

// HIBPSchema is modeled after the HIBP model (ref:
// https://haveibeenpwned.com/API/v3#BreachModel).
type HIBPSchema struct {
	// A Pascal-cased name representing the breach which is unique across all other breaches. This value never changes and may be used to name dependent assets (such as images) but should not be shown directly to end users (see the "Title" attribute instead).
	Name string `json:"Name" validate:"required,Name"`
	// A Pascal-cased name representing the breach which is unique across all other breaches. This value never changes and may be used to name dependent assets (such as images) but should not be shown directly to end users (see the "Title" attribute instead).
	Title string `json:"Title"`
	// The domain of the primary website the breach occurred on. This may be used for identifying other assets external systems may have for the site.
	Domain string `json:"Domain"`
	// The date (with no time) the breach originally occurred on in ISO 8601 format. This is not always accurate — frequently breaches are discovered and reported long after the original incident. Use this attribute as a guide only.
	// YY-MM-DD -> marshal to string, convert to proper time later.
	BreachDate string `json:"BreachDate"`
	// The date and time (precision to the minute) the breach was added to the system in ISO 8601 format.
	AddedDate time.Time `json:"AddedDate"`
	// The date and time (precision to the minute) the breach was modified in ISO 8601 format. This will only differ from the AddedDate attribute if other attributes represented here are changed or data in the breach itself is changed (i.e. additional data is identified and loaded). It is always either equal to or greater then the AddedDate attribute, never less than.
	ModifiedDate time.Time `json:"ModifiedDate"`
	// The total number of accounts loaded into the system. This is usually less than the total number reported by the media due to duplication or other data integrity issues in the source data.
	PwnCount int `json:"PwnCount"`
	// Contains an overview of the breach represented in HTML markup. The description may include markup such as emphasis and strong tags as well as hyperlinks.
	Description string `json:"Description"`
	// This attribute describes the nature of the data compromised in the breach and contains an alphabetically ordered string array of impacted data classes.
	DataClasses []string `json:"DataClasses"`
	// Indicates that the breach is considered unverified. An unverified breach may not have been hacked from the indicated website. An unverified breach is still loaded into HIBP when there's sufficient confidence that a significant portion of the data is legitimate.
	IsVerified bool `json:"IsVerified"`
	// Indicates that the breach is considered fabricated. A fabricated breach is unlikely to have been hacked from the indicated website and usually contains a large amount of manufactured data. However, it still contains legitimate email addresses and asserts that the account owners were compromised in the alleged breach.
	IsFabricated bool `json:"IsFabricated"`
	// Indicates if the breach is considered sensitive. The public API will not return any accounts for a breach flagged as sensitive.
	IsSensitive bool `json:"IsSensitive"`
	// Indicates if the breach has been retired. This data has been permanently removed and will not be returned by the API.
	IsRetired bool `json:"IsRetired"`
	// Indicates if the breach is considered a spam list. This flag has no impact on any other attributes but it means that the data has not come as a result of a security compromise.
	IsSpamList bool `json:"IsSpamList"`
	// Indicates if the breach is sourced from malware. This flag has no impact on any other attributes, it merely flags that the data was sourced from a malware campaign rather than a security compromise of an online service.
	IsMalware bool `json:"IsMalware"`
	// A URI that specifies where a logo for the breached service can be found. Logos are always in PNG format.
	LogoPath string `json:"LogoPath"`
}

// Fields of the HIBP that model the HIBP API v3, ref:
// https://haveibeenpwned.com/API/v3.
func (HIBP) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		// Unique and permanent.
		field.String("name").
			NotEmpty().
			Unique().
			Immutable(),
		// Unique but may change.
		field.String("title").
			Unique(),
		field.String("domain").
			Immutable(),
		field.String("breach_date"),
		// precision to the minute.
		field.Time("added_date"),
		field.Time("modified_date"),
		field.Int("pwn_count"),
		field.String("description").
			Optional().
			Nillable(),
		field.Strings("dataclasses"),
		field.Bool("is_verified").
			Default(false),
		field.Bool("is_fabricated").
			Default(false),
		field.Bool("is_sensitive").
			Default(false),
		field.Bool("is_retired").
			Default(false),
		field.Bool("is_spamList").
			Default(false),
		field.Bool("is_malware").
			Default(false),
		field.String("logo_path").
			Optional().
			Nillable(),
	}
}

// Edges of the HIBP.
func (HIBP) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("tracked_breaches", TrackedBreaches.Type).
			Ref("hibp").
			Unique(),
	}
}

// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Setup holds the schema definition for the Setup entity.
type Setup struct {
	ent.Schema
}

// Fields of the Setup.
func (Setup) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		field.Time("set_up_at").
			Default(time.Now).
			Immutable(),
	}
}

// Edges of the Setup.
func (Setup) Edges() []ent.Edge {
	return nil
}

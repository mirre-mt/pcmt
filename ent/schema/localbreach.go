// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// LocalBreach holds the schema definition for the LocalBreach entity.
type LocalBreach struct {
	ent.Schema
}

// ImportSchema is the LocalBreach model.
type ImportSchema struct {
	Name              string    `yaml:"name" validate:"required,name"`
	Description       string    `yaml:"description"`
	CreatedAt         time.Time `yaml:"createdAt"`
	Date              time.Time `yaml:"time"`
	IsVerified        bool      `yaml:"isVerified"`
	ContainsUsernames bool      `yaml:"containsUsernames"`
	ContainsEmails    bool      `yaml:"containsEmails"`
	ContainsPasswords bool      `yaml:"containsPasswds"`
	ContainsHashes    bool      `yaml:"containsHashes"`
	HashType          string    `yaml:"hashType"`
	HashSalted        bool      `yaml:"hashSalted"`
	HashPeppered      bool      `yaml:"hashPeppered"`
	Data              Data      `yaml:"data"`
}

type (
	DataUsernames []string
	DataEmails    []string
	DataHashes    []string
	DataPasswords []string
	Data          struct {
		Usernames *DataUsernames
		Emails    *DataEmails
		Hashes    *DataHashes
		Passwords *DataPasswords
	}
)

// Fields of the LocalBreach.
func (LocalBreach) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		// Unique but may change.
		field.String("name").
			Unique().
			NotEmpty(),
		field.Time("date").
			Default(time.Now()),
		field.String("description").
			Optional().
			Nillable(),
		field.Bool("is_verified"),
		field.Bool("contains_passwords"),
		field.Bool("contains_hashes"),
		field.Bool("contains_emails"),
		field.Bool("contains_usernames"),
		field.String("hash_type").
			Comment("MD5, SHA1, Argon2id...").
			Optional().
			Nillable(),
		field.Bool("hash_salted").
			Optional().
			Nillable(),
		field.Bool("hash_peppered").
			Optional().
			Nillable(),
		field.JSON("data", &Data{}).
			Optional(),
		field.Time("created_at").
			Default(time.Now()).
			Immutable(),
	}
}

// Edges of the LocalBreach.
func (LocalBreach) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("tracked_breaches", TrackedBreaches.Type).
			Ref("localbreach"),
	}
}

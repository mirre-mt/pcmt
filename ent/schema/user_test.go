// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"regexp"
	"testing"
)

func TestUsernameRegex(t *testing.T) {
	testCases := []struct {
		Username string
		Match    bool
	}{
		{
			Username: "", Match: false,
		},
		{
			Username: "a", Match: false,
		},
		{
			Username: "aa", Match: true,
		},
		{
			Username: "admin", Match: true,
		},
		{
			Username: "+asd+", Match: true,
		},
		{
			Username: "a~", Match: true,
		},
		{
			Username: "a~a", Match: true,
		},
		{
			Username: ".", Match: false,
		},
		{
			Username: "...", Match: false,
		},
		{
			Username: "_____~", Match: true,
		},
		{
			Username: "a_____", Match: true,
		},
		{
			Username: "+_+", Match: true,
		},
		{
			Username: "+ _+", Match: false,
		},
		{
			Username: "+_+ ", Match: false,
		},
		{
			Username: "+_ +", Match: false,
		},
		{
			Username: "", Match: false,
		},
		{
			Username: "dsa dfsa", Match: false,
		},
		{
			Username: " ", Match: false,
		},
		{
			Username: "\t  ", Match: false,
		},
		{
			Username: " <M dsa>", Match: false,
		},
		{
			Username: "<Mdsa>", Match: true,
		},
		{
			Username: "[]", Match: false,
		},
		{
			Username: "[a]", Match: false,
		},
		{
			Username: "a]", Match: false,
		},
		{
			Username: "]", Match: false,
		},
		{
			Username: "[a]]", Match: false,
		},
		{
			Username: "[[a]", Match: false,
		},
		{
			Username: "{}", Match: false,
		},
		{
			Username: "{a}", Match: false,
		},
		{
			Username: "()", Match: false,
		},
		{
			Username: "(a)", Match: false,
		},
	}

	r := regexp.MustCompile(usernameRegex)

	for _, tc := range testCases {
		want := tc.Match
		got := r.MatchString(tc.Username)

		if got != want {
			t.Errorf("mismatch for username %q: want: %t, got: %t", tc.Username, want, got)
		}
	}
}

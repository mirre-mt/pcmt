// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Settings holds the schema definition for the Settings entity.
type Settings struct {
	ent.Schema
}

// Fields of the Settings.
func (Settings) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		field.String("hibp_api_key").
			Comment("api key for the hibp service").
			Sensitive().
			Optional().
			Unique(),
		field.String("dehashed_api_key").
			Comment("api key for the dehashed.com service").
			Sensitive().
			Optional().
			Unique(),
		field.Uint64("searches").
			Comment("the number of searches performed").
			Default(0),
	}
}

// Edges of the Settings.
func (Settings) Edges() []ent.Edge {
	return nil
}

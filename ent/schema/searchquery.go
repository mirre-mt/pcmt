// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// SearchQuery holds the schema definition for the SearchQuery entity.
type SearchQuery struct {
	ent.Schema
}

// Fields of the SearchQuery.
func (SearchQuery) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		field.String("query").
			NotEmpty().
			Immutable(),
		field.Time("created").
			Default(time.Now).
			Immutable(),
		field.UUID("owner", uuid.UUID{}).
			Comment("id of the user that's done the search query").
			Immutable(),
	}
}

// Edges of the SearchQuery.
func (SearchQuery) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).
			Ref("search_queries").
			Field("owner").
			Unique().
			Immutable().
			Required(),
	}
}

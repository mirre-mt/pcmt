// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: AGPL-3.0-only

package schema

import (
	"net/mail"
	"regexp"
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

const usernameRegex = `(^[\S]+)([A-Za-z1-9<>~+_-])$`

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Immutable(),
		field.String("username").
			NotEmpty().
			Unique().
			MinLen(2).
			Match(regexp.MustCompile(usernameRegex)).
			Comment("user's handle, allowed `<>~+-_` symbols and alphanumeric ASCII characters, including capital letters"),
		field.String("email").
			NotEmpty().
			Unique().
			Validate(func(email string) error {
				_, err := mail.ParseAddress(email)
				return err
			}),
		field.Bytes("password").
			Sensitive().
			NotEmpty(),
		field.Bool("is_admin").
			Default(false),
		field.Bool("is_active").
			Default(true),
		field.Time("created_at").
			Default(time.Now).
			Immutable(),
		field.Time("updated_at").
			Default(time.Now).
			UpdateDefault(time.Now),
		field.Time("last_login").
			// UpdateDefault(time.Now).
			Default(time.Unix(0, 0)),
		// field.Bool("save_search_queries").
		// Default(true),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("agekey", AgeKey.Type).
			Unique().
			Immutable(),
		edge.To("tracked_breaches", TrackedBreaches.Type),
		edge.To("search_queries", SearchQuery.Type),
	}
}

// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"git.dotya.ml/mirre-mt/pcmt/ent/searchquery"
	"git.dotya.ml/mirre-mt/pcmt/ent/user"
	"github.com/google/uuid"
)

// SearchQueryCreate is the builder for creating a SearchQuery entity.
type SearchQueryCreate struct {
	config
	mutation *SearchQueryMutation
	hooks    []Hook
}

// SetQuery sets the "query" field.
func (sqc *SearchQueryCreate) SetQuery(s string) *SearchQueryCreate {
	sqc.mutation.SetQuery(s)
	return sqc
}

// SetCreated sets the "created" field.
func (sqc *SearchQueryCreate) SetCreated(t time.Time) *SearchQueryCreate {
	sqc.mutation.SetCreated(t)
	return sqc
}

// SetNillableCreated sets the "created" field if the given value is not nil.
func (sqc *SearchQueryCreate) SetNillableCreated(t *time.Time) *SearchQueryCreate {
	if t != nil {
		sqc.SetCreated(*t)
	}
	return sqc
}

// SetOwner sets the "owner" field.
func (sqc *SearchQueryCreate) SetOwner(u uuid.UUID) *SearchQueryCreate {
	sqc.mutation.SetOwner(u)
	return sqc
}

// SetID sets the "id" field.
func (sqc *SearchQueryCreate) SetID(u uuid.UUID) *SearchQueryCreate {
	sqc.mutation.SetID(u)
	return sqc
}

// SetNillableID sets the "id" field if the given value is not nil.
func (sqc *SearchQueryCreate) SetNillableID(u *uuid.UUID) *SearchQueryCreate {
	if u != nil {
		sqc.SetID(*u)
	}
	return sqc
}

// SetUserID sets the "user" edge to the User entity by ID.
func (sqc *SearchQueryCreate) SetUserID(id uuid.UUID) *SearchQueryCreate {
	sqc.mutation.SetUserID(id)
	return sqc
}

// SetUser sets the "user" edge to the User entity.
func (sqc *SearchQueryCreate) SetUser(u *User) *SearchQueryCreate {
	return sqc.SetUserID(u.ID)
}

// Mutation returns the SearchQueryMutation object of the builder.
func (sqc *SearchQueryCreate) Mutation() *SearchQueryMutation {
	return sqc.mutation
}

// Save creates the SearchQuery in the database.
func (sqc *SearchQueryCreate) Save(ctx context.Context) (*SearchQuery, error) {
	sqc.defaults()
	return withHooks(ctx, sqc.sqlSave, sqc.mutation, sqc.hooks)
}

// SaveX calls Save and panics if Save returns an error.
func (sqc *SearchQueryCreate) SaveX(ctx context.Context) *SearchQuery {
	v, err := sqc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (sqc *SearchQueryCreate) Exec(ctx context.Context) error {
	_, err := sqc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (sqc *SearchQueryCreate) ExecX(ctx context.Context) {
	if err := sqc.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (sqc *SearchQueryCreate) defaults() {
	if _, ok := sqc.mutation.Created(); !ok {
		v := searchquery.DefaultCreated()
		sqc.mutation.SetCreated(v)
	}
	if _, ok := sqc.mutation.ID(); !ok {
		v := searchquery.DefaultID()
		sqc.mutation.SetID(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (sqc *SearchQueryCreate) check() error {
	if _, ok := sqc.mutation.Query(); !ok {
		return &ValidationError{Name: "query", err: errors.New(`ent: missing required field "SearchQuery.query"`)}
	}
	if v, ok := sqc.mutation.Query(); ok {
		if err := searchquery.QueryValidator(v); err != nil {
			return &ValidationError{Name: "query", err: fmt.Errorf(`ent: validator failed for field "SearchQuery.query": %w`, err)}
		}
	}
	if _, ok := sqc.mutation.Created(); !ok {
		return &ValidationError{Name: "created", err: errors.New(`ent: missing required field "SearchQuery.created"`)}
	}
	if _, ok := sqc.mutation.Owner(); !ok {
		return &ValidationError{Name: "owner", err: errors.New(`ent: missing required field "SearchQuery.owner"`)}
	}
	if _, ok := sqc.mutation.UserID(); !ok {
		return &ValidationError{Name: "user", err: errors.New(`ent: missing required edge "SearchQuery.user"`)}
	}
	return nil
}

func (sqc *SearchQueryCreate) sqlSave(ctx context.Context) (*SearchQuery, error) {
	if err := sqc.check(); err != nil {
		return nil, err
	}
	_node, _spec := sqc.createSpec()
	if err := sqlgraph.CreateNode(ctx, sqc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	if _spec.ID.Value != nil {
		if id, ok := _spec.ID.Value.(*uuid.UUID); ok {
			_node.ID = *id
		} else if err := _node.ID.Scan(_spec.ID.Value); err != nil {
			return nil, err
		}
	}
	sqc.mutation.id = &_node.ID
	sqc.mutation.done = true
	return _node, nil
}

func (sqc *SearchQueryCreate) createSpec() (*SearchQuery, *sqlgraph.CreateSpec) {
	var (
		_node = &SearchQuery{config: sqc.config}
		_spec = sqlgraph.NewCreateSpec(searchquery.Table, sqlgraph.NewFieldSpec(searchquery.FieldID, field.TypeUUID))
	)
	if id, ok := sqc.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = &id
	}
	if value, ok := sqc.mutation.Query(); ok {
		_spec.SetField(searchquery.FieldQuery, field.TypeString, value)
		_node.Query = value
	}
	if value, ok := sqc.mutation.Created(); ok {
		_spec.SetField(searchquery.FieldCreated, field.TypeTime, value)
		_node.Created = value
	}
	if nodes := sqc.mutation.UserIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   searchquery.UserTable,
			Columns: []string{searchquery.UserColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(user.FieldID, field.TypeUUID),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.Owner = nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// SearchQueryCreateBulk is the builder for creating many SearchQuery entities in bulk.
type SearchQueryCreateBulk struct {
	config
	builders []*SearchQueryCreate
}

// Save creates the SearchQuery entities in the database.
func (sqcb *SearchQueryCreateBulk) Save(ctx context.Context) ([]*SearchQuery, error) {
	specs := make([]*sqlgraph.CreateSpec, len(sqcb.builders))
	nodes := make([]*SearchQuery, len(sqcb.builders))
	mutators := make([]Mutator, len(sqcb.builders))
	for i := range sqcb.builders {
		func(i int, root context.Context) {
			builder := sqcb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*SearchQueryMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				var err error
				nodes[i], specs[i] = builder.createSpec()
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, sqcb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, sqcb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, sqcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (sqcb *SearchQueryCreateBulk) SaveX(ctx context.Context) []*SearchQuery {
	v, err := sqcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (sqcb *SearchQueryCreateBulk) Exec(ctx context.Context) error {
	_, err := sqcb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (sqcb *SearchQueryCreateBulk) ExecX(ctx context.Context) {
	if err := sqcb.Exec(ctx); err != nil {
		panic(err)
	}
}

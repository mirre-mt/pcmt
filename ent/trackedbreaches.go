// Code generated by ent, DO NOT EDIT.

package ent

import (
	"fmt"
	"strings"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
	"git.dotya.ml/mirre-mt/pcmt/ent/trackedbreaches"
	"git.dotya.ml/mirre-mt/pcmt/ent/user"
	"github.com/google/uuid"
)

// TrackedBreaches is the model entity for the TrackedBreaches schema.
type TrackedBreaches struct {
	config `json:"-"`
	// ID of the ent.
	ID uuid.UUID `json:"id,omitempty"`
	// Breach holds the value of the "breach" field.
	Breach uuid.UUID `json:"breach,omitempty"`
	// Whether the breach is a locally added one or obtained from an online service
	Online bool `json:"online,omitempty"`
	// id of the user tracking the breach
	Owner uuid.UUID `json:"owner,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the TrackedBreachesQuery when eager-loading is set.
	Edges        TrackedBreachesEdges `json:"edges"`
	selectValues sql.SelectValues
}

// TrackedBreachesEdges holds the relations/edges for other nodes in the graph.
type TrackedBreachesEdges struct {
	// User holds the value of the user edge.
	User *User `json:"user,omitempty"`
	// Localbreach holds the value of the localbreach edge.
	Localbreach []*LocalBreach `json:"localbreach,omitempty"`
	// Hibp holds the value of the hibp edge.
	Hibp []*HIBP `json:"hibp,omitempty"`
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [3]bool
}

// UserOrErr returns the User value or an error if the edge
// was not loaded in eager-loading, or loaded but was not found.
func (e TrackedBreachesEdges) UserOrErr() (*User, error) {
	if e.loadedTypes[0] {
		if e.User == nil {
			// Edge was loaded but was not found.
			return nil, &NotFoundError{label: user.Label}
		}
		return e.User, nil
	}
	return nil, &NotLoadedError{edge: "user"}
}

// LocalbreachOrErr returns the Localbreach value or an error if the edge
// was not loaded in eager-loading.
func (e TrackedBreachesEdges) LocalbreachOrErr() ([]*LocalBreach, error) {
	if e.loadedTypes[1] {
		return e.Localbreach, nil
	}
	return nil, &NotLoadedError{edge: "localbreach"}
}

// HibpOrErr returns the Hibp value or an error if the edge
// was not loaded in eager-loading.
func (e TrackedBreachesEdges) HibpOrErr() ([]*HIBP, error) {
	if e.loadedTypes[2] {
		return e.Hibp, nil
	}
	return nil, &NotLoadedError{edge: "hibp"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*TrackedBreaches) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case trackedbreaches.FieldOnline:
			values[i] = new(sql.NullBool)
		case trackedbreaches.FieldID, trackedbreaches.FieldBreach, trackedbreaches.FieldOwner:
			values[i] = new(uuid.UUID)
		default:
			values[i] = new(sql.UnknownType)
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the TrackedBreaches fields.
func (tb *TrackedBreaches) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case trackedbreaches.FieldID:
			if value, ok := values[i].(*uuid.UUID); !ok {
				return fmt.Errorf("unexpected type %T for field id", values[i])
			} else if value != nil {
				tb.ID = *value
			}
		case trackedbreaches.FieldBreach:
			if value, ok := values[i].(*uuid.UUID); !ok {
				return fmt.Errorf("unexpected type %T for field breach", values[i])
			} else if value != nil {
				tb.Breach = *value
			}
		case trackedbreaches.FieldOnline:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field online", values[i])
			} else if value.Valid {
				tb.Online = value.Bool
			}
		case trackedbreaches.FieldOwner:
			if value, ok := values[i].(*uuid.UUID); !ok {
				return fmt.Errorf("unexpected type %T for field owner", values[i])
			} else if value != nil {
				tb.Owner = *value
			}
		default:
			tb.selectValues.Set(columns[i], values[i])
		}
	}
	return nil
}

// Value returns the ent.Value that was dynamically selected and assigned to the TrackedBreaches.
// This includes values selected through modifiers, order, etc.
func (tb *TrackedBreaches) Value(name string) (ent.Value, error) {
	return tb.selectValues.Get(name)
}

// QueryUser queries the "user" edge of the TrackedBreaches entity.
func (tb *TrackedBreaches) QueryUser() *UserQuery {
	return NewTrackedBreachesClient(tb.config).QueryUser(tb)
}

// QueryLocalbreach queries the "localbreach" edge of the TrackedBreaches entity.
func (tb *TrackedBreaches) QueryLocalbreach() *LocalBreachQuery {
	return NewTrackedBreachesClient(tb.config).QueryLocalbreach(tb)
}

// QueryHibp queries the "hibp" edge of the TrackedBreaches entity.
func (tb *TrackedBreaches) QueryHibp() *HIBPQuery {
	return NewTrackedBreachesClient(tb.config).QueryHibp(tb)
}

// Update returns a builder for updating this TrackedBreaches.
// Note that you need to call TrackedBreaches.Unwrap() before calling this method if this TrackedBreaches
// was returned from a transaction, and the transaction was committed or rolled back.
func (tb *TrackedBreaches) Update() *TrackedBreachesUpdateOne {
	return NewTrackedBreachesClient(tb.config).UpdateOne(tb)
}

// Unwrap unwraps the TrackedBreaches entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (tb *TrackedBreaches) Unwrap() *TrackedBreaches {
	_tx, ok := tb.config.driver.(*txDriver)
	if !ok {
		panic("ent: TrackedBreaches is not a transactional entity")
	}
	tb.config.driver = _tx.drv
	return tb
}

// String implements the fmt.Stringer.
func (tb *TrackedBreaches) String() string {
	var builder strings.Builder
	builder.WriteString("TrackedBreaches(")
	builder.WriteString(fmt.Sprintf("id=%v, ", tb.ID))
	builder.WriteString("breach=")
	builder.WriteString(fmt.Sprintf("%v", tb.Breach))
	builder.WriteString(", ")
	builder.WriteString("online=")
	builder.WriteString(fmt.Sprintf("%v", tb.Online))
	builder.WriteString(", ")
	builder.WriteString("owner=")
	builder.WriteString(fmt.Sprintf("%v", tb.Owner))
	builder.WriteByte(')')
	return builder.String()
}

// TrackedBreachesSlice is a parsable slice of TrackedBreaches.
type TrackedBreachesSlice []*TrackedBreaches

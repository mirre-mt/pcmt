-- Create "hib_ps" table
CREATE TABLE "hib_ps" ("id" uuid NOT NULL, "name" character varying NOT NULL, "domain" character varying NOT NULL, "breach_date" timestamptz NOT NULL, "added_date" timestamptz NOT NULL, "modified_date" timestamptz NOT NULL, "pwn_count" bigint NOT NULL, "description" character varying NOT NULL, "dataclasses" jsonb NOT NULL, "is_verified" boolean NOT NULL DEFAULT false, "is_fabricated" boolean NOT NULL DEFAULT false, "is_sensitive" boolean NOT NULL DEFAULT false, "is_retired" boolean NOT NULL DEFAULT false, "is_spam_list" boolean NOT NULL DEFAULT false, "is_malware" boolean NOT NULL DEFAULT false, "logo" character varying NOT NULL, PRIMARY KEY ("id"));
-- Create index "hib_ps_name_key" to table: "hib_ps"
CREATE UNIQUE INDEX "hib_ps_name_key" ON "hib_ps" ("name");
-- Create "setups" table
CREATE TABLE "setups" ("id" uuid NOT NULL, "set_up_at" timestamptz NOT NULL, PRIMARY KEY ("id"));
-- Create "users" table
CREATE TABLE "users" ("id" uuid NOT NULL, "username" character varying NOT NULL, "email" character varying NOT NULL, "password" bytea NOT NULL, "is_admin" boolean NOT NULL DEFAULT false, "is_active" boolean NOT NULL DEFAULT true, "created_at" timestamptz NOT NULL, "updated_at" timestamptz NOT NULL, PRIMARY KEY ("id"));
-- Create index "users_email_key" to table: "users"
CREATE UNIQUE INDEX "users_email_key" ON "users" ("email");
-- Create index "users_username_key" to table: "users"
CREATE UNIQUE INDEX "users_username_key" ON "users" ("username");

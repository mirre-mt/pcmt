-- create with NULL in order to allow backfilling.
ALTER TABLE "users" ADD COLUMN "last_login" timestamptz DEFAULT '1970-01-01 00:00:00+00' NULL;
-- backfill...
UPDATE "users" SET "last_login"='1970-01-01 00:00:00+00' where "last_login" is NULL;
-- set NOT NULL.
ALTER TABLE "users" ALTER COLUMN "last_login" SET NOT NULL;

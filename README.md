# [`pcmt`](https://git.dotya.ml/mirre-mt/pcmt/)

> Password Compromise Monitoring Tool

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/mirre-mt/pcmt/status.svg)](https://drone.dotya.ml/mirre-mt/pcmt)
[![Go Report Card](https://goreportcard.com/badge/git.dotya.ml/mirre-mt/pcmt)](https://goreportcard.com/report/git.dotya.ml/mirre-mt/pcmt)
[![Go Documentation](https://godocs.io/git.dotya.ml/mirre-mt/pcmt?status.svg)](https://godocs.io/git.dotya.ml/mirre-mt/pcmt)
[![Docker Image Version](https://img.shields.io/docker/v/immawanderer/mt-pcmt/linux-amd64)](https://hub.docker.com/r/immawanderer/mt-pcmt/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/immawanderer/mt-pcmt/linux-amd64)](https://hub.docker.com/r/immawanderer/mt-pcmt/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker pulls](https://img.shields.io/docker/pulls/immawanderer/mt-pcmt)](https://hub.docker.com/r/immawanderer/mt-pcmt/)

> :construction: **note:** this project is being developed as a part of my
> [thesis](https://git.dotya.ml/mirre-mt/masters-thesis/) and is currently
> a work in progress. :construction:

<p align="center">
  <figure align="center">
      <img
      src="https://git.dotya.ml/mirre-mt/pcmt/raw/branch/development/assets/public/img/logo-pcmt.svg"
      alt="pcmt Gopher logo"
      />
      <figcaption>pcmt Gopher logo based on the Egon Elbre's <a href="https://github.com/egonelbre/gophers" target="_blank">awesome Gopher designs</a>.</figcaption>
  </figure>
</p>


### Who is this good for

it might come in handy to anybody willing to operate this application for
family, a group of friends, a university faculty or even just for themselves.

### What even is this

Password Compromise Monitoring Tool offers a frictionless way to check
credentials for potential compromise. of course, the application cannot
possibly know it all, which is why no indication of compromise does not
necessarily amount to no compromise.

### How to try this out

with [`podman`](https://podman.io/) and [`just`](https://github.com/casey/just)
installed, run the following, only supplanting the `mt-pcmt:<tag>` with either
`testbuild` or one of the [recent tagged
releases](https://git.dotya.ml/mirre-mt/pcmt/releases):
```sh
# build the image locally using kaniko.
just kaniko

# start postgres.
just dbstart

# in another terminal, run the application.
# LiveMode=False disables loading assets and templates from the filesystem and
# instead uses the embedded resources.
podman run --rm -it -e PCMT_DBTYPE=postgres \
    -e PCMT_CONNSTRING="host=127.0.0.1 port=5432 sslmode=disable dbname=postgres password=postgres"
    -e PCMT_LIVE=False docker.io/immawanderer/mt-pcmt:<tag> \
    -config /etc/pcmt/config.dhall
```

while the above runs *fine*, running in **pods** is *better* and more close to
a production setting (assuming
[rootless](https://www.redhat.com/sysadmin/rootless-podman-user-namespace-modes)
[Podman](https://www.redhat.com/sysadmin/rootless-containers-podman) setup):
```sh
# create a pod 🦭.
podman pod create --userns=keep-id -p3005:3000 --name pcmt

# if you have the db from the previous example still running, terminate it by
# pressing ^C or running the following:
just dbstop

# run a db in the pod.
podman run --pod pcmt --replace -d --name "pcmt-pg" --rm \
    -e POSTGRES_INITDB_ARGS="--auth-host=scram-sha-256 --auth-local=scram-sha-256" \
    -e POSTGRES_PASSWORD=postgres -v $PWD/tmp/db:/var/lib/postgresql/data \
    docker.io/library/postgres:15.3-alpine3.18

# run the application in the pod (assuming that you have built it as in the
# previous example). do note that we're connecting to the db using its
# container name, while localhost would also work. inside the pod, every
# container is reachable on localhost.
podman run --pod pcmt --replace --name pcmt-og -d --rm \
    -e PCMT_LIVE=False \
    -e PCMT_DBTYPE="postgres" \
    -e PCMT_CONNSTRING="host=pcmt-pg port=5432 sslmode=disable user=postgres dbname=postgres password=postgres" \
    -v $PWD/config.dhall:/config.dhall:ro \
    docker.io/immawanderer/mt-pcmt:<tag> -config /config.dhall

# also, if we try to connect to the db from the host we get an error (unless
# there is another database running on localhost already, such as the one from
# the previous example).

curl localhost:5432
--> curl: (7) Failed to connect to localhost port 5432 after 0 ms: Couldn't connect to server

# that is because the database port has not been exposed from the pod (recall
# the pod creation command).
```

#### Custom config

Make sure to check out the Dhall configuration
[schema](https://git.dotya.ml/mirre-mt/pcmt-config-schema/src/branch/development/schema.dhall)
to see what's possible, or have a look at the [example
config](exampleConfig.dhall).

If you're new to Dhall, its [documentation](https://docs.dhall-lang.org/) page
is a good resource to start at.

### Run modes

there are toggles that affect how the application behaves, and they can both be
supplied either as environment variables prefixed with `PCMT_` or in the
configuration file:
* `LiveMode`
* `DevelMode`

live mode makes the application load all assets from persistent storage, i.e.
it **no** assets embedded at build time are used (so make sure to supply your
own).

devel mode is mainly useful in development or when debugging. it makes the
application reload the templates at every request and automatically increases
log levels.

### 🔨 Building from sources

pre-requisites:
* [`just`](https://github.com/casey/just) (for convenience)
* [`npm`](https://docs.npmjs.com/cli/v9/)
* [`go1.20`](https://go.dev/)
* `git`

the application consists of *frontend* and *backend*. the *frontend* is
basically a **generated** TailwindCSS stylesheet and it is not shipped as part
of the development process. as such, it needs to be built from sources just the
same as the backend.

**step 0:** clone this repository with `git`

**step 1:** build the *frontend* using `just tw` or `just frontend`

**step 2:** finally, build the Go application using:
```sh
# debugging version.
just build

# or with debugging information stripped.
just buildrelease
```

**step 1-alt:** build both *frontend* and *backend* in release modes using:
`just prod`.

the order of the steps is important as the application embeds the generated
stylesheet.

if you plan to run the application in `LiveMode`, the stylesheet can be
supplied when running the application; however, the binary will lack the way to
fall back to the embedded asset so bear that in mind.

if you're curious, you can open the [`justfile`](justfile) to see the details
of the build targets (such as `tw` and `buildrelease`) used above.


### :balance_scale: LICENSE
AGPL-3.0-only (see [LICENSE](LICENSE) for details).
